import React, {Fragment, useState} from 'react'

const EditTodo = ({user}) => {
    const [cid, setCid] = useState(user.cid);
    const [number, setNumber] = useState(user.number);
    const [email, setEmail] = useState(user.email);
    const [password, setPassword] = useState(user.password);
    
    //edit user
    const updateUser = async (e) => {
        e.preventDefault();
        
        try {
            const body = {cid, number, email, password};
            const response = await fetch(`http://localhost:5000/admin/${user.cid}`, {
                method: "PUT",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify(body)
            })
            // console.log(response)
            window.location = "/"
        } catch (err) {
            console.error(err.message)
        }
    };
    
    //change data
    const changeData = () => {
        setCid(user.cid)
        setNumber(user.number)
        setEmail(user.email)
        setPassword(user.password)
    }

  return (
    <Fragment>
        {/* Button trigger modal */}
        <button type="button" className="btn-sm btn-info" data-bs-toggle="modal" data-bs-target={`#id${user.cid}`}>
        edit
        </button>

        {/* Modal */}
        <div className="modal fade" id={`id${user.cid}`} data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div className="modal-dialog">
            <div className="modal-content">
            <div className="modal-header">
                <h5 className="modal-title" id="staticBackdropLabel">Edit User</h5>
                <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div className="modal-body d-flex gap-2">
                <label className="form-label">CID:</label>
                <input type="number" className="form-control" value={cid} onChange={e => setCid(e.target.value)}></input>
            </div>
            <div className="modal-body d-flex gap-2">
                <label className="form-label">Number:</label>
                <input type="number" className="form-control" value={number} onChange={e => setNumber(e.target.value)}></input>
            </div>
            <div className="modal-body d-flex gap-2">
                <label className="form-label">Email:</label>
                <input type="email" className="form-control" value={email} onChange={e => setEmail(e.target.value)}></input>
            </div>
            <div className="modal-body d-flex gap-2">
                <label className="form-label">Password:</label>
                <input type="text" className="form-control" value={password} onChange={e => setPassword(e.target.value)}></input>
            </div>
            <div className="modal-footer">
                <button type="button" className="btn btn-warning" data-bs-dismiss="modal" onClick={changeData}>Close</button>
                <button type="button" className="btn btn-danger"
                    onClick={e => updateUser(e)}
                >Update</button>
            </div>
            </div>
        </div>
        </div>
    </Fragment>
  )
}

export default EditTodo