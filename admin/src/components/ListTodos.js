import React, {Fragment, useEffect, useState} from 'react'
import EditTodo from './EditTodo'

const ListTodos = () => {

    const [user, setUser] = useState([])

    //delete user
    const deleteUser = async id => {
        try {
            const deleteUser = await fetch(`http://localhost:5000/admin/${id}`, {
                method : "DELETE"
            });
            // console.log(deleteUser)
            // window.location = "/";
            setUser(user.filter(user => user.cid !==id));
            
        } catch (err) {
            console.error(err)    
        }
    }

    //fetch users
    const getUser = async() => {
        try {
            const response = await fetch("http://localhost:5000/admin");
            const jsonData = await response.json();
            // console.log(jsonData);
            setUser(jsonData);
            
        } catch (err) {
            console.error(err.message);   
        }
    }

    useEffect(() => {
        getUser();
    }, []);

    // console.log(user);
  return (
    <Fragment>
    <table className="table table-striped text-center mt-5">
        <thead>
            <tr>
            <th scope="col">CID</th>
            <th scope="col">NUMBER</th>
            <th scope="col">EMAIL</th>
            <th scope="col">PASSWORD</th>
            <th scope="col">ACTION</th>
            
            </tr>
        </thead>
        <tbody>
            {user.map( u => (
                <tr key={u.cid}>
                    <th scope="row">{u.cid}</th>
                    <td>{u.number}</td>
                    <td>{u.email}</td>
                    <td>{u.password}</td>
                    <td className="d-flex gap-2">
                        <EditTodo user={u}/>
                        <button className="btn-sm btn-danger" 
                            onClick={() => deleteUser(u.cid)}>delete</button>
                    </td>
                </tr>

            ))}

        </tbody>
        </table>

    </Fragment>
  )
}

export default ListTodos;