import React, {Fragment, useState} from 'react'

const InputTodo = () => {

    const [cid, setCid] = useState("")
    const [number, setNumber] = useState("")
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")

    const onSubmitForm = async e => {
        e.preventDefault();
        try {
            const body = {cid, number, email, password};
            const response = await fetch("http://localhost:5000/admin", {
                method: "POST",
                headers: { "Content-Type": "application/json"},
                body: JSON.stringify(body)
            });
            // console.log(response);
            window.location = "/";
        } catch (err) {
            console.error(err.message)
            
        }
    }
  return (
    <Fragment>
        <h1 className="text-center mt-5">Pern Todo List</h1>
        <form className="mt-5" onSubmit={onSubmitForm}>
            <div className="mb-3">
                <label className="form-label">CID:</label>
                <input type="number" className="form-control" value={cid} 
                onChange={e => setCid(e.target.value)}/>
            </div>

            <div className="mb-3">
                <label className="form-label">NUMBER:</label>
                <input type="number" className="form-control" value={number} 
                onChange={e => setNumber(e.target.value)}/>
            </div>

            <div className="mb-3">
                <label className="form-label">EMAIL:</label>
                <input type="email" className="form-control" value={email} 
                onChange={e => setEmail(e.target.value)}/>
            </div>

            <div className="mb-5">
                <label className="form-label">PASSWORD:</label>  
                <input type="password" className="form-control" value={password} 
                onChange={e => setPassword(e.target.value)}/>
            </div>

            <button type="submit" className="btn btn-primary">Submit</button>
        </form>
    </Fragment>
  )
}

export default InputTodo