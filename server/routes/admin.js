const pool = require("../db")
const router = require("express").Router();
const { ip } = require('../utils/config');
const multer = require("multer");
const path = require("path");

// storage engine 

const storage = multer.diskStorage({
    destination: './upload/images',
    filename: (req, file, cb) => {
        return cb(null, `${file.fieldname}_${Date.now()}${path.extname(file.originalname)}`)
    }
})

const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1000000
    }
    
})

router.post("/upload", upload.single('img'), (req, res) => {
    res.json({
        success: 1,
        profile_url: `http://localhost:4000/img/${req.file.filename}`
    })
})

router.post("/upload", upload.single('img'), (req, res)=>{
    console.log(req.file)
})

//create a todo
router.post("/", async(req, res)=> {
    try {
        // console.log(req.body)
        const {cid, number, email, password} = req.body;
        const newUser = await pool.query(
            "INSERT INTO userr (cid, number, email, password) VALUES($1, $2, $3, $4) RETURNING *",
            [cid, number, email, password]
    );
    res.json(newUser.rows[0]);

    } catch (err) {
        console.error(err.message);
    }
})

//get all
router.get("/", async(req, res) => {
    try {
        const allUser = await pool.query("SELECT * FROM userr")
        res.json(allUser.rows)
    } catch (err) {
        console.error(err.messsage)
        
    }
})

//get 
router.get("/:id", async(req, res) => {
    try {
        // console.log(req.params)
        const { id } = req.params;
        const user = await pool.query("SELECT * FROM userr WHERE cid = $1", [id])

        res.json(user.rows[0])

    } catch (err) {
        console.error(err.message)  
    }
});

//update
router.put("/:id", async(req, res) => {
    try {
        const { id } = req.params;
        const { number, email, password } = req.body;
        const updateUser = await pool.query(
            "UPDATE userr SET number = $1, email = $2, password = $3 WHERE cid = $4",
            [number, email, password, id]
        );
        res.json({ message: "User updated successfully" });

    } catch (err) {
        console.error(err.message)
    }
})

//delete
router.delete("/:id", async(req, res) => {
    try {
        const { id } = req.params;
        const deleteUser = await pool.query("DELETE FROM userr WHERE cid =$1", [id])
        
        res.json("user was deleted!");
    } catch (err) {
        console.error(err.message)
    }
})

//add collector



//add area
router.post("/area", upload.single('img'), async (req, res) => {
    const client = await pool.connect();
    try {
        await client.query('BEGIN'); // Begin the transaction

        const { name, totalSlot, light_price, twoWheeler_price, descrip } = req.body;
        const profileUrl = `http://${ip}:5000/img/${req.file.filename}`; // Construct profile URL

        // Perform both insertions within the same transaction
        const newArea = await client.query(
            "INSERT INTO area (area, totalSlot, light_price, twoWheeler_price, img, descrip) VALUES ($1, $2, $3, $4, $5, $6) RETURNING *",
            [name, totalSlot, light_price, twoWheeler_price, profileUrl, descrip]
        );

        const newCheckParking = await client.query(
            "INSERT INTO checkarea (a_id, availableSlots) VALUES ($1, $2) RETURNING *",
            [newArea.rows[0].a_id, totalSlot]
        );

        await client.query('COMMIT'); // Commit the transaction

        res.json({ success: 1, profile_url: profileUrl, newArea: newArea.rows[0], newCheckParking: newCheckParking.rows[0] });
    } catch (err) {
        await client.query('ROLLBACK'); // Rollback the transaction if there's an error
        console.error(err.message);
        res.status(500).json('Internal server error');
    } finally {
        client.release(); // Release the client back to the pool
    }
});


//multer error handler
function errHandler(err, req, res, next) {
    if (err instanceof multer.MulterError) {
        res.json({
            success: 0,
            message: err.message
        })
    }
}
router.use(errHandler);

module.exports = router