const router = require("express").Router();
const pool = require("../db")
const bcrypt = require("bcrypt");
const authorization = require("../middleware/authorization")
const { Expo } = require('expo-server-sdk');
const expo = new Expo();


router.get("/", authorization, async (req, res) => {
    try {
        //req.user has the payload

        // const user = await pool.query(
        //     "SELECT * FROM users WHERE user_id = $1",
        //     [req.user.id] 
        //   );

        const user = await pool.query(
          "SELECT * FROM users AS u LEFT JOIN vehicle AS v ON u.user_id = v.user_id WHERE u.user_id = $1",
          [req.user.id]
        );
          
        //if would be req.user if you change your payload to this:
          
        //   function jwtGenerator(user_id) {
        //   const payload = {
        //     user: user_id
        //   };
          
          res.json(user.rows[0]);
        
    } catch (err) {
        console.error(err.message);
        res.status(500).json("Server Error! "+err.message);
    }
})

//get profile
router.get("/profile", authorization, async (req, res) => {
  try {
      const profile = await pool.query(
        "SELECT * from users WHERE user_id = $1",
        [req.user.id]
      );
      
      res.json(profile.rows[0]);
      
  } catch (err) {
      console.error(err.message);
      res.status(500).json("Server Error! "+err.message);
  }
});

//get Vehicle
router.get("/vehicle", authorization, async (req, res) => {
  try {
      const vehicle = await pool.query(
        "SELECT * from vehicle WHERE user_id = $1",
        [req.user.id]
      );
      
      res.json(vehicle.rows);
      
  } catch (err) {
      console.error(err.message);
      res.status(500).json("Server Error! "+err.message);
  }
});

//update profile
//change username
router.put("/username", authorization, async (req, res) => {
  try {
    const { name } = req.body;

    // Update the user's name and password in the database
    const updateName = await pool.query(
      "UPDATE users SET user_name = $1 WHERE user_id = $2 RETURNING *",
      [name,req.user.id]
    );

    if (updateName.rows.length === 0) {
      return res.status(400).json("This profile is not yours");
    }

    res.json("Name was updated");
  } catch (err) {
    console.error(err.message);
    res.status(500).json("Server Error! "+err.message);
  }
});
//change password
router.put("/password", authorization, async (req, res) => {
  try {
    const { newPassword, oldPassword } = req.body;

    // Get the current password from the database
    const currentPass = await pool.query(
      "SELECT password FROM users WHERE user_id = $1",
      [req.user.id]
    );

    // If the current password provided by the user does not match the hashed password stored in the database, return an error
    const passwordMatch = await bcrypt.compare(oldPassword, currentPass.rows[0].password);
    if (!passwordMatch) {
      return res.status(400).json("Incorrect current password");
    }

    // Bcrypt the new password
    const saltRounds = 10;
    const salt = await bcrypt.genSalt(saltRounds);
    const hashedPassword = await bcrypt.hash(newPassword, salt);

    // Update the user's name and password in the database
    const updatePassword = await pool.query(
      "UPDATE users SET password = $1 WHERE user_id = $2 RETURNING *",
      [hashedPassword, req.user.id]
    );

    if (updatePassword.rows.length === 0) {
      return res.status(400).json("This profile is not yours");
    }

    res.json("Passord was updated");
  } catch (err) {
    console.error(err.message);
    res.status(500).json("Server Error! "+err.message);
  }
});

//get vehicle
router.get("/vehicle", authorization, async (req, res) => {
  try {
      const vehicle = await pool.query(
        "SELECT * from vehicle AS u WHERE u.user_id = $1",
        [req.user.id]
      );
      
      res.json(vehicle.rows);
      
  } catch (err) {
      console.error(err.message);
      res.status(500).json("Server Error! "+err.message);
  }
});

//create vehicle
router.post("/addVehicle", authorization, async (req, res) => {
  try {
    const { vehicleNo, vehicleType } = req.body;

    // Check if the vehicle number already exists in the database
    const existingVehicle = await pool.query(
      "SELECT * FROM vehicle WHERE vehicleno = $1",
      [vehicleNo]
    );

    if (existingVehicle.rows.length > 0) {
      return res.status(400).json("Vehicle number already exists");
    }

    // Insert the new vehicle if it doesn't already exist
    const newVehicle = await pool.query(
      "INSERT INTO vehicle (vehicleno, type, user_id) VALUES ($1, $2, $3) RETURNING *",
      [vehicleNo, vehicleType, req.user.id]
    );

    res.json(newVehicle.rows[0]);
  } catch (err) {
    console.error(err.message);
    res.status(500).json("Server Error! "+err.message);
  }
});

//update vehicle
router.put("/vehicle/:v_id", authorization, async (req, res) => {
  try {
    const { v_id } = req.params;
    const { vehicleNo, vehicleType } = req.body;
    const updateVehicle = await pool.query(
      "UPDATE vehicle SET vehicleno = $1, type = $2 WHERE v_id = $3 AND user_id = $4 RETURNING *",
      [vehicleNo, vehicleType, v_id, req.user.id]
    );

    if (updateVehicle.rows.length === 0) {
      return res.json("This vehicle is not yours");
    }

    res.json("vehicle was updated");
  } catch (err) {
    console.error(err.message);
  }
});

//delete vehicle
router.delete("/vehicle/:v_id", authorization, async (req, res) => {
  try {
    const { v_id } = req.params;
    const deleteVehicle = await pool.query(
      "DELETE FROM vehicle WHERE v_id = $1 AND user_id = $2 RETURNING *",
      [v_id, req.user.id]
    );

    if (deleteVehicle.rows.length === 0) {
      return res.json("This vehicle is not yours");
    }

    res.json("vehicle was deleted");
  } catch (err) {
    console.error(err.message);
  }
});

//get area
router.get("/area", authorization, async (req, res) => {
  try {
      const area = await pool.query(
        "SELECT * FROM area LEFT JOIN checkarea ON area.a_id = checkarea.a_id"
      );
      
      res.json(area.rows);
      
  } catch (err) {
      console.error(err.message);
      res.status(500).json("Server Error! "+err.message);
  }
});

//get parking status
router.get("/status", authorization, async (req, res) => {
  try {
      const status = await pool.query(
        "SELECT * FROM parking WHERE user_id = $1",
        [req.user.id]
      );
      
      res.json(status.rows);
      
  } catch (err) {
      console.error(err.message);
      res.status(500).json("Server Error! "+err.message);
  }
});

//create parking
router.post("/parking", authorization, async (req, res) => {
  const client = await pool.connect();
  try {
    await client.query('BEGIN'); // Begin the transaction

    const { name, vehicleNo, a_id} = req.body;

    //get user name
    const user_name_query = await client.query(
      "SELECT user_name FROM users WHERE user_id = $1",
      [req.user.id]
    );

    const user_name = user_name_query.rows[0].user_name;

     // Check if the user already has a parking record
     const existingParking = await client.query(
      "SELECT * FROM parking WHERE user_id = $1",
      [req.user.id]
    );

    // If the user already has a parking record, return an error
    if (existingParking.rows.length > 0) {
      return res.status(400).json("You already parked your car");
    }

    // Insert into parking table
    const newParking = await client.query(
      "INSERT INTO parking (user_id, area, vehicleNo, user_name, a_id) VALUES ($1, $2, $3, $4, $5) RETURNING *",
      [req.user.id, name, vehicleNo, user_name, a_id]
    );

    //get available slots
    const slot_query = await client.query(
      "SELECT availableslots FROM checkarea WHERE a_id = $1",
      [a_id]
    );

    const available_slot = slot_query.rows[0].availableslots;

    // Calculate available slots
    const slot = available_slot - 1;

    // Update availability in checkarea table
    await client.query(
      "UPDATE checkarea SET availableslots = $1, last_updated = CURRENT_TIME AT TIME ZONE 'Asia/Thimphu' WHERE a_id = $2",
      [slot, a_id]
    );

    await client.query('COMMIT'); // Commit the transaction
    res.json(newParking.rows[0]);

  } catch (err) {
    await client.query('ROLLBACK'); // Rollback the transaction if there's an error
    console.error(err.message);
    res.status(500).json("Server Error! "+err.message);
  } finally {
    client.release(); // Release the client back to the pool
  }
});

//notify collector
// router.post("/notify", authorization, async (req, res) => {
//   const client = await pool.connect();
//   try {
//     await client.query('BEGIN');

//     const { user_name, user_id, starttime, area, vehicleno, a_id, expoPushToken } = req.body;

//     // Validate input parameters
//     if (!user_name || !user_id || !starttime || !area || !vehicleno || !a_id) {
//       return res.status(400).json("Missing required parameters");
//     }

//     // Delete parking
//     const deleteParking = await client.query(
//       "DELETE FROM parking WHERE user_id = $1 RETURNING *",
//       [req.user.id]
//     );

//     if (deleteParking.rows.length === 0) {
//       await client.query('ROLLBACK');
//       return res.status(403).json("Parking entry not found for the user");
//     }

//     // Fetch available slot
//     const slotResult = await client.query(
//       "SELECT availableslots FROM checkarea WHERE a_id = $1",
//       [a_id]
//     );

//     if (slotResult.rows.length === 0) {
//       await client.query('ROLLBACK');
//       return res.status(404).json("Area not found");
//     }

//     const available_slot = slotResult.rows[0].availableslots;

//     // Calculate available slots
//     const slot = available_slot + 1;

//     // Update availability in checkarea table
//     await client.query(
//       "UPDATE checkarea SET availableslots = $1, last_updated = CURRENT_TIME AT TIME ZONE 'Asia/Thimphu' WHERE a_id = $2",
//       [slot, a_id]
//     );

//     // Insert new notification
//     const newNotification = await client.query(
//       "INSERT INTO notification (user_name, user_id, starttime, area, vehicleno, a_id) VALUES ($1, $2, $3, $4, $5, $6) RETURNING *",
//       [user_name, user_id, starttime, area, vehicleno, a_id]
//     );

//     await client.query('COMMIT');
//     res.json(newNotification.rows[0]);

//   } catch (err) {
//     console.error(err);
//     await client.query('ROLLBACK');
//     res.status(500).json("Server Error! "+err.message );
//   } finally {
//     client.release();
//   }
// });
router.post("/notify", authorization, async (req, res) => {
  const client = await pool.connect();
  try {
    await client.query('BEGIN');

    const { user_name, user_id, starttime, area, vehicleno, a_id, expoPushToken } = req.body;

    // Validate input parameters
    if (!user_name || !user_id || !starttime || !area || !vehicleno || !a_id || !expoPushToken) {
      return res.status(400).json("Missing required parameters");
    }

    // Delete parking
    const deleteParking = await client.query(
      "DELETE FROM parking WHERE user_id = $1 RETURNING *",
      [req.user.id]
    );

    if (deleteParking.rows.length === 0) {
      await client.query('ROLLBACK');
      return res.status(403).json("Parking entry not found for the user");
    }

    // Fetch available slot
    const slotResult = await client.query(
      "SELECT availableslots FROM checkarea WHERE a_id = $1",
      [a_id]
    );

    if (slotResult.rows.length === 0) {
      await client.query('ROLLBACK');
      return res.status(404).json("Area not found");
    }

    const available_slot = slotResult.rows[0].availableslots;

    // Calculate available slots
    const slot = available_slot + 1;

    // Update availability in checkarea table
    await client.query(
      "UPDATE checkarea SET availableslots = $1, last_updated = CURRENT_TIME AT TIME ZONE 'Asia/Thimphu' WHERE a_id = $2",
      [slot, a_id]
    );

    // Insert new notification
    const newNotification = await client.query(
      "INSERT INTO notification (user_name, user_id, starttime, area, vehicleno, a_id, expo_push_token) VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING *",
      [user_name, user_id, starttime, area, vehicleno, a_id, expoPushToken]
    );

    await client.query('COMMIT');
    res.json(newNotification.rows[0]);

  } catch (err) {
    console.error(err);
    await client.query('ROLLBACK');
    res.status(500).json("Server Error! "+err.message );
  } finally {
    client.release();
  }
});




//get History
router.get("/history", authorization, async (req, res) => {
  try {
      const history = await pool.query(
        "SELECT * FROM history WHERE user_id = $1",
        [req.user.id]
      );

      res.json(history.rows);
      
  } catch (err) {
      console.error(err.message);
      res.status(500).json("Server Error! "+err.message);
  }
});

module.exports = router;