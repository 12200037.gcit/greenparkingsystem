const router = require("express").Router();
const pool = require("../db");
const bcrypt = require("bcrypt");
const jwtGenerator = require("../utils/jwtGenerator");
const validInfo = require("../middleware/validInfo");
const authorization = require("../middleware/authorization");

//registering

router.post("/register", validInfo, async (req, res) => {
    const client = await pool.connect(); // Acquire a client from the pool
    try {
        await client.query('BEGIN'); // Start a transaction

        // Destructure the req.body
        const { name, phoneNo, email, type, password, vehicleNo, vehicleType } = req.body;

        // Check if user exists (based on email or phoneNo)
        const user = await client.query("SELECT * FROM users WHERE email = $1 OR phoneNo = $2", [email, phoneNo]);

        if (user.rows.length > 0) {
            await client.query('ROLLBACK'); // Rollback transaction if user already exists
            return res.status(401).json("User already exists!");
        }

        // Check if vehicle number is repeated
        const existingVehicle = await client.query('SELECT * FROM vehicle WHERE vehicleno = $1', [vehicleNo]);

        if (existingVehicle.rows.length > 0) {
            await client.query('ROLLBACK'); // Rollback transaction if vehicle number is already in use
            return res.status(401).json("Vehicle number is already in use!");
        }

        // Bcrypt the user password
        const saltRound = 10;
        const salt = await bcrypt.genSalt(saltRound);
        const bcryptPassword = await bcrypt.hash(password, salt);

        // Insert the new user inside our database
        const newUser = await client.query(
            "INSERT INTO users (user_name, phoneNo, email, type, password) VALUES ($1, $2, $3, $4, $5) RETURNING user_id",
            [name, phoneNo, email, type, bcryptPassword]
        );

        const userId = newUser.rows[0].user_id;

        // Insert the vehicle into the database
        await client.query('INSERT INTO vehicle (vehicleno, type, user_id) VALUES ($1, $2, $3)', [vehicleNo, vehicleType, userId]);

        await client.query('COMMIT'); // Commit the transaction if everything is successful

        // Generate JWT token
        const jwtToken = jwtGenerator(userId);

        return res.json({ jwtToken });
    } catch (err) {
        await client.query('ROLLBACK'); // Rollback the transaction if any error occurs
        console.error(err.message);
        res.status(500).send("Server Error");
    } finally {
        client.release(); // Release the client back to the pool
    }
});


// Login route

//logic user
router.post("/login", validInfo, async (req, res) => {
    try {
        //1. destructure the req.body
        const {phoneNo, email, password} = req.body

        //2. check if user doesn't exist (if not then we throw error)
        const user = await pool.query("SELECT * FROM users WHERE email = $1 OR phoneNo = $2", [
            email,
            phoneNo
        ]);

        if (user.rows.length === 0) {
            return res.status(401).json("Invalid Credential");
        }
          
        //3. check if incoming password is the same as the database password
        const validPassword = await bcrypt.compare(
            password,
            user.rows[0].password
        );
        
        if (!validPassword) {
            return res.status(401).json("Invalid Credential");
        }

        //4. give them the jwt token
        const jwtToken = jwtGenerator(user.rows[0].user_id);
        return res.json({ jwtToken });

    } catch (err) {
        console.error(err.message)
        res.status(500).send("Server Error")
    }
})

//login collector
router.post("/loginCollector", validInfo, async (req, res) => {
    try {
        //1. destructure the req.body
        const {phoneNo, password} = req.body

        //2. check if user doesn't exist (if not then we throw error)
        const user = await pool.query("SELECT * FROM collector WHERE phoneno = $1", [
            phoneNo
        ]);

        if (user.rows.length === 0) {
            return res.status(401).json("Invalid Credential");
        }
          
        //3. check if incoming password is the same as the database password
        const validPassword = await bcrypt.compare(
            password,
            user.rows[0].password
        );
        
        if (!validPassword) {
            return res.status(401).json("Invalid Credential");
        }

        //4. give them the jwt token
        const jwtToken = jwtGenerator(user.rows[0].collector_id);
        return res.json({ jwtToken });

    } catch (err) {
        console.error(err.message)
        res.status(500).send("Server Error")
    }
})

router.get("/verify", authorization, async (req, res) => {
    try {
        res.json(true);
    } catch (err) {
        console.error(err.message);
        res.status(500).send("Server Error");
        
    }
})

//add collector
router.post("/registerC", validInfo, async (req, res) => {
    try {
        // Destructure the req.body
        const { name, phoneNo, area, password} = req.body;

        // Check if collector exists 
        const collector = await pool.query("SELECT * FROM collector WHERE phoneno = $1", [phoneNo]);

        if (collector.rows.length > 0) {
            return res.status(401).json("Collector already exists!");
        }

        // Bcrypt the user password
        const saltRound = 10;
        const salt = await bcrypt.genSalt(saltRound);
        const bcryptPassword = await bcrypt.hash(password, salt);

        // Insert the new user inside our database
        const newCollector = await pool.query(
            "INSERT INTO collector (collector_name, phoneno, area, password) VALUES ($1, $2, $3, $4) RETURNING *",
            [name, phoneNo, area, bcryptPassword]
        );

        // Generate JWT token
        const jwtToken = jwtGenerator(newCollector.rows[0].newCollector_id);

        return res.json({ jwtToken });
    } catch (err) {
        console.error(err.message);
        res.status(500).send("Server Error");
    }
});

//login collector
router.post("/loginC", validInfo, async (req, res) => {
    try {
        //1. destructure the req.body
        const {phoneNo, password} = req.body

        //2. check if collector doesn't exist (if not then we throw error)
        const collector = await pool.query("SELECT * FROM collector WHERE phoneno = $1", [phoneNo]);

        if (collector.rows.length === 0) {
            return res.status(401).json("Invalid Credential");
        }
          
        //3. check if incoming password is the same as the database password
        const validPassword = await bcrypt.compare(
            password,
            collector.rows[0].password
        );
        
        if (!validPassword) {
            return res.status(401).json("Invalid Credential");
        }

        //4. give them the jwt token
        const jwtToken = jwtGenerator(collector.rows[0].collector_id);
        return res.json({ jwtToken });

    } catch (err) {
        console.error(err.message)
        res.status(500).send("Server Error")
    }
})

module.exports = router;