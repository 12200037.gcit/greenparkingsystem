const router = require("express").Router();
const pool = require("../db")
const authorization = require("../middleware/authorization")

router.get("/", authorization, async (req, res) => {
    try { 
      
        const user = await pool.query(
          "SELECT * FROM collector WHERE collector_id = $1",
          [req.user.id]
        );
          
          res.json(user.rows[0]);
        
    } catch (err) {
        console.error(err.message);
        res.status(500).json("Server Error! "+err.message);
    }
});

//get area
router.get("/area", authorization, async (req, res) => {
  try {

    //get user name
    const area_query = await pool.query(
      "SELECT area FROM collector WHERE collector_id = $1",
      [req.user.id]
    );

    if (area_query.rows.length === 0) {
      // If no rows are returned, send an appropriate response
      return res.status(404).json("Area not found!");
    }

    const area_name = area_query.rows[0].area;

      const area = await pool.query(
        "SELECT * FROM area LEFT JOIN checkarea ON area.a_id = checkarea.a_id WHERE area = $1",
        [area_name]
      );
      
      res.json(area.rows);
      
  } catch (err) {
      console.error(err.message);
      res.status(500).json("Server Error! "+err.message);
  }
});

//get parking status
router.get("/status", authorization, async (req, res) => {
  try {

    // Fetch the area name for the current user
    const areaResult = await pool.query(
      "SELECT area FROM collector WHERE collector_id = $1",
      [req.user.id]
    );

    const area_name = areaResult.rows[0].area;
    
    // Query the parking status based on the area name
    const statusResult = await pool.query(
      "SELECT * FROM parking WHERE area = $1 ORDER BY starttime DESC",
      [area_name]
    );
    
    res.json(statusResult.rows);
  } catch (err) {
    console.error(err.message);
    res.status(500).json("Server Error! "+err.message);
  }
});

//cancel parking
router.post("/cancel/:p_id", authorization, async (req, res) => {
  const client = await pool.connect();

  try {
    await client.query("BEGIN");

    const { p_id } = req.params;
    const { selectedReason, a_id, user_id, user_name } = req.body;

    // Validation
    if (!selectedReason || !user_id || !user_name) {
      await client.query("ROLLBACK");
      return res.status(400).json("Missing required data");
    }

    // Delete parking
    const deleteParking = await client.query(
      "DELETE FROM parking WHERE p_id = $1 RETURNING *",
      [p_id]
    );

    if (deleteParking.rows.length === 0) {
      await client.query("ROLLBACK");
      return res.status(403).json("This is not your parking");
    }

    // Update availability
    const slot_query = await client.query(
      "SELECT availableslots FROM checkarea WHERE a_id = $1",
      [a_id]
    );

    if (slot_query.rows.length === 0) {
      await client.query('ROLLBACK');
      return res.status(404).json("Slot not found");
    }

    const available = slot_query.rows[0].availableslots;
    const slot = available + 1;

    await client.query(
      "UPDATE checkarea SET availableslots = $1, last_updated = CURRENT_TIME AT TIME ZONE 'Asia/Thimphu' WHERE a_id = $2 RETURNING *",
      [slot, a_id]
    );

    // Get collector name
    const nameQuery = await client.query(
      "SELECT area FROM collector WHERE collector_id = $1",
      [req.user.id]
    );
    const name = nameQuery.rows[0].area;

    // report
    await client.query(
      "INSERT INTO report (collector_id, collector_name, user_id, user_name, report) VALUES ($1, $2, $3, $4, $5) RETURNING *",
      [req.user.id, name, user_id, user_name, selectedReason]
    );

    await client.query("COMMIT");
    res.json("Parking was cancelled");
  } catch (err) {
    await client.query("ROLLBACK");
    console.error("Error in canceling parking:", err);
    res.status(500).json("Internal Server Error! "+err.message);
  } finally {
    client.release();
  }
});

//get Notification
router.get("/notification", authorization, async (req, res) => {
  try {

    // Fetch the area name for the current user
    const areaResult = await pool.query(
      "SELECT area FROM collector WHERE collector_id = $1",
      [req.user.id]
    );

    const area_name = areaResult.rows[0].area;
    // Query the parking status based on the area name
    const notification = await pool.query(
      "SELECT * FROM notification WHERE area = $1 ORDER BY endtime",
      [area_name]
    );
    
    res.json(notification.rows);
  } catch (err) {
    console.error(err.message);
    res.status(500).json("Server Error! "+err.message);
  }
});

//Done Payment and history
router.post("/payment/:n_id", authorization, async (req, res) => {
  const client = await pool.connect();
  try {
    await client.query('BEGIN');

    const { n_id } = req.params;
    const {a_id, area, user_id, user_name, vehicleno, starttime, endtime} = req.body;
    
    // remove notification
    const removeNoti = await client.query(
      "DELETE FROM notification WHERE n_id = $1 RETURNING *",
      [n_id]
    );

    if (removeNoti.rows.length === 0) {
      await client.query('ROLLBACK');
      return res.status(403).json("No Notification");
    }

    // history
    // Get collector name
    const nameQuery = await client.query(
      "SELECT collector_name FROM collector WHERE collector_id = $1",
      [req.user.id]
    );
    const name = nameQuery.rows[0].area;

    await client.query( 
      "INSERT INTO history (a_id, area, user_id, user_name, vehicleno, collector_id, collector_name, starttime, endtime ) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING *",
      [a_id, area, user_id, user_name, vehicleno, req.user.id, name, starttime, endtime]
    );

    await client.query('COMMIT');
    res.status(200).json("Payment successful");

  } catch (err) {
    await client.query('ROLLBACK');
    console.error(err.message);
    res.status(500).json("Server Error! " + err.message);
  } finally {
    client.release();
  }
});

//get History
router.get("/history", authorization, async (req, res) => {
  try {
      const history = await pool.query(
        "SELECT * FROM history WHERE collector_id = $1  ORDER BY endtime DESC", //AND date=CURRENT_DATE
        [req.user.id]
      );

      res.json(history.rows);
      
  } catch (err) {
      console.error(err.message);
      res.status(500).json("Server Error! "+err.message);
  }
});

module.exports = router;