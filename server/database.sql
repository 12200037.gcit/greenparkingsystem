CREATE DATABASE gps;

--user
ALTER TABLE current_table_name
RENAME TO new_table_name;

--join
select * from users INNER JOIN vehicle ON users.user_id = vehicle.user_id WHERE users.phoneNo = 'd';

-- create extension if not exists "uuid-ossp"; extension need for uuid
DROP TABLE IF EXISTS users;

CREATE TABLE users(
    user_id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    user_name VARCHAR(255) NOT NULL,
    phoneno VARCHAR(255) UNIQUE,
    email VARCHAR(255) UNIQUE,
    type VARCHAR(255) DEFAULT 'user' CHECK (type IN ('user', 'subscriber', 'gov')),
    password VARCHAR(255) NOT NULL
);
insert into users (name, phoneNo, email, type, password) VALUES ('Tenzin', '111', 'Rigsar@gmail.com', 'password');

CREATE TABLE collector(
    collector_id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    collector_name VARCHAR(255) NOT NULL,
    phoneno VARCHAR(255) UNIQUE,
    area VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL
);

--vehicle
CREATE TABLE vehicle(
    v_id SERIAL PRIMARY KEY,
    vehicleno VARCHAR(255),
    type VARCHAR(255) DEFAULT 'light' CHECK (type IN ('light', 'twoWheeler')),
    user_id UUID REFERENCES "users"(user_id) ON DELETE CASCADE
);
INSERT INTO vehicle (number, type, user_id) VALUES ('ABC123', 'light', 111);

--area
CREATE TABLE area(
    a_id SERIAL PRIMARY KEY,
    area VARCHAR(255) NOT NULL,
    totalslot INTEGER NOT NULL,
    light_price NUMERIC(10, 2) NOT NULL,
    twowheeler_price NUMERIC(10, 2) NOT NULL,
    img VARCHAR(255) NOT NULL, 
    descrip VARCHAR(255)  
);

--check area availability
CREATE TABLE checkarea(
    c_id SERIAL PRIMARY KEY,
    a_id INTEGER REFERENCES area(a_id) ON DELETE CASCADE,
    availableslots INTEGER NOT NULL,
    last_updated TIME DEFAULT (CURRENT_TIME AT TIME ZONE 'Asia/Thimphu')
);

--current parking
CREATE TABLE parking(
    p_id SERIAL PRIMARY KEY,
    user_name VARCHAR(255) NOT NULL,
    user_id UUID REFERENCES "users"(user_id) ON DELETE CASCADE,
    area VARCHAR(255) NOT NULL,
    a_id INTEGER REFERENCES area(a_id) ON DELETE CASCADE,
    vehicleno VARCHAR(255) NOT NULL,
    starttime TIME DEFAULT (CURRENT_TIME AT TIME ZONE 'Asia/Thimphu')
);

--notification
CREATE TABLE notification (
    n_id SERIAL PRIMARY KEY,
    user_name VARCHAR(255) NOT NULL,
    user_id UUID REFERENCES "users" (user_id) ON DELETE CASCADE,
    area VARCHAR(255) NOT NULL,
    a_id INTEGER REFERENCES area(a_id) ON DELETE CASCADE,
    vehicleno VARCHAR(255) NOT NULL,
    starttime TIME, 
    endtime TIME DEFAULT (CURRENT_TIME AT TIME ZONE 'Asia/Thimphu'), 
    duration TIME GENERATED ALWAYS AS (endtime - starttime) STORED,
    expo_push_token VARCHAR(255) NOT NULL
);


--history
CREATE TABLE history(
    h_id SERIAL PRIMARY KEY,
    user_id UUID REFERENCES "users"(user_id) ON DELETE CASCADE,
    collector_id UUID REFERENCES "collector"(collector_id) ON DELETE CASCADE,
    user_name VARCHAR(255),
    collector_name VARCHAR(255),
    area VARCHAR(255),
    vehicleno VARCHAR(255) NOT NULL,
    a_id INTEGER REFERENCES area(a_id) ON DELETE CASCADE,
    starttime TIME,
    endtime TIME,
    duration TIME GENERATED ALWAYS AS (endtime - starttime) STORED, 
    totalamount NUMERIC(10),
    paymenttype VARCHAR(255) DEFAULT 'scan' CHECK (paymenttype IN ('scan', 'cash')),
    date DATE DEFAULT CURRENT_DATE
);
-- Inserting values into the history table
INSERT INTO history (user_id, collector_id, user_name, collector_name, area, vehicleno, a_id, date, starttime, endtime, duration, totalamount, paymenttype)
VALUES 
  ('971e0918-66c2-490d-9713-c4bba94913ab', '226d4e00-4ee8-416b-b00b-9e6505023c16', 'Dpp', 'Gaybo','z', 'BP-14', '21', '2024-05-06T18:00:00.000Z', '23:16:51', '23:30:00', '02:30:00', 10, 'online');

--feedback
CREATE TABLE feedback(
    f_id SERIAL PRIMARY KEY,
    user_id UUID REFERENCES "users"(user_id) ON DELETE CASCADE,
    collector_id UUID REFERENCES "collector"(collector_id) ON DELETE CASCADE,
    user_name VARCHAR(255),
    collector_name VARCHAR(255),
    feedback VARCHAR(255),
    starttime TIME DEFAULT (CURRENT_TIME AT TIME ZONE 'Asia/Thimphu'),
    date DATE DEFAULT CURRENT_DATE
);

--report
CREATE TABLE report(
    r_id SERIAL PRIMARY KEY,
    collector_id UUID REFERENCES "collector"(collector_id) ON DELETE CASCADE,
    user_id UUID REFERENCES "users"(user_id) ON DELETE CASCADE,
    user_name VARCHAR(255),
    collector_name VARCHAR(255),
    report VARCHAR(255),
    starttime TIME DEFAULT (CURRENT_TIME AT TIME ZONE 'Asia/Thimphu'),
    date DATE DEFAULT CURRENT_DATE
);

car number BT-1-A4593
