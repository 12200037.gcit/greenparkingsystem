const jwt = require("jsonwebtoken");
require("dotenv").config();

module.exports = async(req, res, next) => {
    try {
        // get token
        const jwtToken = req.header("token");
        // console.log(jwtToken)

        // Check if not token
        if (!jwtToken) {
            console.log("ur token is not working")
            return res.status(403).json("Not Authorize");
        }
        
        // Verify token
        const payload = jwt.verify(jwtToken, process.env.jwtSecret);
        // console.log("ur token working")
        req.user = payload.user;
        next();
    } catch (err) {
        console.error(err.message);
        return res.status(403).json("Not Authorize")
        
    }
}