module.exports = function(req, res, next) {
    const {phoneNo, email, name, type, password, vehicleNo, vehicleType } = req.body;
  
    function validEmail(userEmail) {
      return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(userEmail);
    }

    function validPhoneNumber(phoneNumber) {
          // Check if the phone number is empty or contains only whitespace
        if (!phoneNumber.trim()) {
            return false; // Phone number is empty or contains only whitespace
        }
        return true;
    }
  
    if (req.path === "/register") {
        // Check if the required fields (name, type, password) are missing
        if (![name, type, password, vehicleNo, vehicleType].every(Boolean)) {
            return res.status(401).json("Missing Credentials");
        }
    
        // Check if both email and phoneNo are empty
        if (!email && !phoneNo) {
            return res.status(401).json("Email or Phone Number is required");
        }
    
        // Validate email if provided
        if (email && !validEmail(email)) {
            return res.status(401).json("Invalid Email");
        }
    
        // Validate phone number if provided
        if (phoneNo && !validPhoneNumber(phoneNo)) {
            return res.status(401).json("Invalid Phone Number");
        }
    }    
    else if (req.path === "/login") {
        // Check if either email or phoneNo is missing
        if (![password].every(Boolean) || (!email && !phoneNo)) {
            return res.status(401).json("Email or phone number is missing");
        }
    
        // Validate email if provided
        if (email && !validEmail(email)) {
            return res.status(401).json("Invalid Email");
        }
    
        // Validate phone number if provided
        if (phoneNo && !validPhoneNumber(phoneNo)) {
            return res.status(401).json("Invalid Phone Number");
        }
    
        // Your other login logic here...
    
    }
    
    next();
  };