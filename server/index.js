const express = require("express");
const app = express();
const cors = require("cors");

//middleware
app.use(cors());
app.use(express.json()) //req.body

// ROUTES//

//register and login routes
app.use("/auth", require("./routes/jwtAuth"));

// user route
app.use("/user", require("./routes/user"));

//collector route
app.use("/collector", require("./routes/collector"));

//admin route
app.use("/admin", require("./routes/admin"));

//img route(expose those image)
app.use('/img', express.static('./upload/images'));

app.listen(5000, () => {
    console.log("server has started on port 5000");
})