import React, { useState } from 'react';
import { View, TextInput, TouchableOpacity, Text, StatusBar, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { useNavigation } from '@react-navigation/native';

const ForgotPasswordPage = () => {
  const [username, setUsername] = useState('');
  const [usernameError, setUsernameError] = useState('');
  const navigation = useNavigation();

  const handleResetPassword = () => {
    // Implement password reset logic here
    // For this example, let's just navigate back to the login page
    navigation.goBack();
  };

  const validateUsername = () => {
    setUsernameError('');
  
     // Flag to indicate if username is a valid phone number
     let isPhoneNumber = false;

     // Validation for empty username
     if (!username.trim()) {
       setUsernameError('Enter your email or phone number.');
       return false;
     }
 
     // Validation for numeric input
     if (!isNaN(username)) {
       // Check if numeric input is a valid phone number
       const phoneNumber = username.trim();
       if (phoneNumber.length !== 8) {
         setUsernameError('Enter a valid 8 digits phone number');
         return false;
       }
       const phoneRegex = /^(17|77)\d{6}$/;
       if (!phoneRegex.test(phoneNumber)) {
         setUsernameError('Start the Phone Number with 17/77');
         return false;
       }
       isPhoneNumber = true;
     }
 
     // Validation for email
     const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
     if (!isPhoneNumber && !emailRegex.test(username)) {
       setUsernameError('Enter a valid email address.');
       return false;
     }
  
    return true;
  };
  

  const handleResetPasswordPress = () => {
    if (validateUsername()) {
      handleResetPassword();
    }
  };

  return (
    <View style={styles.container}>
      <StatusBar barStyle="light-content" backgroundColor="#1A1B18" />
      <Text style={styles.header}>ENTER YOUR PHONE NUMBER OR EMAIL ADDRESS TO RESET YOUR PASSWORD</Text>
      <View style={styles.inputContainer}>
        <TextInput
          style={styles.input}
          placeholder="Email Address/ Phone Number"
          placeholderTextColor="rgba(255, 255, 255, 0.5)"
          value={username}
          autoCapitalize="none"
          onChangeText={(text) => setUsername(text)}
        />
      </View>
      {usernameError ? <Text style={styles.errorText}>{usernameError}</Text> : null}
      <TouchableOpacity style={styles.buttonContainer} onPress={handleResetPasswordPress}>
        <Text style={styles.buttonText}>Reset Password</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 20,
    backgroundColor: '#1A1B18',
  },
  header: {
    textAlign: 'center',
    color: 'white',
    fontSize: 15,
    fontWeight: '500',
    marginBottom: 30,
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 49,
    borderColor: '#242422',
    borderWidth: 1,
    marginBottom: 20,
    position: 'relative',
  },
  input: {
    height: 49,
    borderColor: '#242422',
    paddingLeft: 30,
    borderWidth: 1,
    backgroundColor: '#242422',
    color: '#FFFFFF',
    flex: 1,
  },
  buttonContainer: {
    backgroundColor: '#CBE877',
    paddingVertical: 15,
    borderRadius: 40,
    marginBottom: 20,
  },
  buttonText: {
    color: '#1A1B18',
    fontSize: 18,
    textAlign: 'center',
    fontWeight: '600',
  },
  errorText: {
    color: '#FF0000',
    marginTop: -15,
    marginLeft: 30,
    marginBottom:20,
  },
});

export default ForgotPasswordPage;
