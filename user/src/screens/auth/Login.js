import React, { useState } from 'react';
import { View, TextInput, TouchableOpacity, Text, StatusBar, StyleSheet, Image, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome'
import AsyncStorage from '@react-native-async-storage/async-storage'; // Import AsyncStorage
import Toast from 'react-native-toast-message';
import {ip} from '../../utils/config'

const LoginPage = ({ navigation, route}) => {
  const { setIsAuthenticated } = route.params
  
  // State variables for email, phone number, password, and password visibility
  const [emailOrPhoneNo, setEmailOrPhoneNo] = useState('');
  const [password, setPassword] = useState('');
  const [isPasswordVisible, setPasswordVisible] = useState(false);

  const [emailOrPhoneNoError, setEmailOrPhoneNoError] = useState('');
  const [passwordError, setPasswordError] = useState('');

  const handleLogin = async () => {
    try {
     
      // if (!validateLogin()) return;
      let requestBody = {};

      // Check if the input contains '@' to determine if it's an email
      if (emailOrPhoneNo.includes('@')) {
        requestBody = { email: emailOrPhoneNo, password };
      } else {
        requestBody = { phoneNo: emailOrPhoneNo, password };
      }

      const response = await fetch(`http://${ip}:5000/auth/login`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(requestBody),
      });
  
      if (!response.ok) {
        const errorData = await response.json();
        throw new Error(errorData); 
      }
  
      const data = await response.json();
          
      // Handle successful login
      await AsyncStorage.setItem('token', data.jwtToken);
      setIsAuthenticated(true)

      navigation.navigate('HomeS', {setIsAuthenticated});
      // Alert.alert('Login Successful', 'Hey Welcome');
      Toast.show({
        type: 'success',
        text1: 'Successfully logged in',
        visibilityTime: 3000,
      });
    } catch (error) {
      // Alert.alert('Login Error', error.message);
      Toast.show({
        type: 'error',
        text1: 'Login Error',
        text2:error.message
      });
    }
  };
  
  const validateLogin = () => {
    setEmailOrPhoneNoError('');
    setPasswordError('');

    // Flag to indicate if username is a valid phone number
    let isPhoneNumber = false;

    // Validation for empty username
    if (!emailOrPhoneNo.trim()) {
      setEmailOrPhoneNoError('Enter your email or phone number.');
      return false;
    }

    // Validation for numeric input
    if (!isNaN(emailOrPhoneNo)) {
      // Check if numeric input is a valid phone number
      const phoneNumber = emailOrPhoneNo.trim();
      if (phoneNumber.length !== 8) {
        setEmailOrPhoneNoError('Enter a valid 8 digits phone number');
        return false;
      }
      const phoneRegex = /^(17|77)\d{6}$/;
      if (!phoneRegex.test(phoneNumber)) {
        setEmailOrPhoneNoError('Start the Phone Number with 17/77');
        return false;
      }
      isPhoneNumber = true;
    }

    // Validation for email
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!isPhoneNumber && !emailRegex.test(emailOrPhoneNo)) {
      setEmailOrPhoneNoError('Enter a valid email address.');
      return false;
    }

    // Validation for empty password
    if (!password.trim()) {
      setPasswordError('Please enter your password.');
      return false;
    }

    // If all validations pass, return true
    return true;
  };
  
  // Function to toggle password visibility
  const togglePasswordVisibility = () => {
    setPasswordVisible(!isPasswordVisible);
  };

  return (
    <View style={styles.container}>
      <StatusBar barStyle="light-content" backgroundColor="#1A1B18" />
      <View style={styles.imageContainer}>
        <Image
          source={require('../../../assets/Login.jpeg')} 
          style={styles.image}
          resizeMode="contain"
        />
      </View>
      <View>
        <Text style={styles.login}>Login to your account</Text>
      </View>
      <View style={styles.inputContainer}>
        <Icon name="user" size={20} color="rgba(255, 255, 255, 0.5)" style={styles.inputIcon} />
        <TextInput
          style={styles.input}
          placeholder="Phone Number / Email Address"
          placeholderTextColor="rgba(255, 255, 255, 0.5)"
          value={emailOrPhoneNo}
          onChangeText={(text) => setEmailOrPhoneNo(text)}
        />
      </View>
      {emailOrPhoneNoError ? <Text style={styles.errorText}>{emailOrPhoneNoError}</Text> : null}

      <View style={styles.inputContainer}>
        <Icon name="lock" size={20} color="rgba(255, 255, 255, 0.5)" style={styles.inputIcon} />
        <TextInput
          style={styles.input}
          placeholder="Password"
          placeholderTextColor="rgba(255, 255, 255, 0.5)"
          secureTextEntry={!isPasswordVisible}
          value={password}
          onChangeText={(text)=> setPassword(text)}
        />
  
        <TouchableOpacity style={styles.eyeIcon} onPress={togglePasswordVisibility}>
          <Icon name={isPasswordVisible ? "eye" : "eye-slash"} size={20} color="rgba(255, 255, 255, 0.5)" />
        </TouchableOpacity>
      </View>

      {passwordError ? <Text style={styles.errorText}>{passwordError}</Text> : null}
      <TouchableOpacity onPress={()=>navigation.navigate('ForgotPassword')}>
        <Text style={styles.forgotPassword}>Forgot Password?</Text>
      </TouchableOpacity>

      {/* login button */}
      <TouchableOpacity style={styles.buttonContainer} onPress={handleLogin}>
        <Text style={styles.buttonText}>Login</Text>
      </TouchableOpacity>

      <View style={styles.signupContainer}>
        <Text style={styles.signupPrompt}>Don't have an account? </Text>
        <TouchableOpacity onPress={() => navigation.navigate('SignUp')}>
          <Text style={styles.highlight}>Sign Up</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 20,
    backgroundColor: '#1A1B18',
  },
  imageContainer: {
    alignItems: 'center',
    marginBottom: 20,
  },
  image: {
    aspectRatio: 1,
    borderBottomLeftRadius:50,
    borderBottomRightRadius:50,
  },
  login: {
    textAlign:'center',
    color:'white',
    fontSize:15,
    fontWeight:'600',
    marginBottom:30,
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 49,
    borderColor: '#242422',
    borderWidth: 1,
    marginBottom: 20,
  },
  inputIcon: {
    position: 'absolute',
    left: 20, 
    zIndex: 1,
    backgroundColor: '#242422',
    paddingHorizontal: 10,
    borderRadius: 5,
  },
  input: {
    height: 49, 
    borderColor: '#242422',
    paddingLeft: 60, 
    borderWidth: 1,
    backgroundColor: '#242422',
    color: '#FFFFFF',
    flex: 1,
  },
  eyeIcon: {
    position: 'absolute',
    right: 20,
    zIndex: 1,
  },
  forgotPassword: {
    color: 'rgba(255, 255, 255, 0.5)',
    textAlign: 'right',
    marginBottom: 20,
    marginTop:-10,
  },
  buttonContainer: {
    backgroundColor: '#CBE877',
    paddingVertical: 15,
    borderRadius: 20,
    marginBottom: 20,
  },
  buttonText: {
    color: '#1A1B18',
    fontSize: 18,
    textAlign: 'center',
    fontWeight: '600',
  },
  signupContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop:-10,
    marginBottom:50,
  },
  signupPrompt: {
    color: '#818181',
  },
  highlight: {
    color: '#CBE877',
    fontWeight: 'bold',
  },
  errorText: {
    color: '#FF0000',
    marginTop: -15,
    marginLeft: 60,
    marginBottom:20,
  },
});

export default LoginPage;