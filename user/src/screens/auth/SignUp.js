import React, { useState } from 'react';
import { ScrollView, View, Alert,TextInput, TouchableOpacity, StyleSheet, Text, SafeAreaView, StatusBar, Switch } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Picker } from '@react-native-picker/picker';
import { MaterialIcons } from '@expo/vector-icons'; 
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Toast from 'react-native-toast-message';
import { ip } from '../../utils/config';

const CustomSwitch = ({ value, onValueChange }) => {
  return (
    <TouchableOpacity
      style={styles.switchContainer}
      onPress={() => onValueChange(!value)}
    >
      <View style={[styles.switch, value ? styles.switchActive : null]} />
    </TouchableOpacity>
  );
};

const SignupPage = ({navigation, route}) => {

  const {setIsAuthenticated} = route.params;

  const [name, setName] = useState('');
  const [emailOrPhoneNo, setEmailOrPhoneNo] = useState('')
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  
  const [vehicleNo, setVehicleNo] = useState('');
  const [vehicleType, setVehicleType] = useState('light');
  const [isForeigner, setIsForeigner] = useState(false);
  
  const [showPassword, setShowPassword] = useState(false); 
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);
  const type = "user"; 

  const [nameError, setNameError] = useState('');
  const [phoneError, setPhoneError] = useState('');
  const [vehicleNoError, setVehicleNoError] = useState('');
  const [passwordError, setPasswordError] = useState('');
  const [confirmPasswordError, setConfirmPasswordError] = useState('');
  const [vehicleTypeError, setVehicleTypeError] = useState('');
  const [emailError, setEmailError] = useState('');
  
  const handleSignup = async () => {
    try {

      //validate from clientside
      // if (!validateSignup()) return;

      let requestBody = {};
  
      // Check if the user is a foreigner
      if (isForeigner) {
        // If user is a foreigner, treat emailOrPhoneNo as an email
        requestBody = { email: emailOrPhoneNo, name, vehicleNo, vehicleType, type, password };
      } else {
        // If user is Bhutanese, treat emailOrPhoneNo as a phone number
        requestBody = { phoneNo: emailOrPhoneNo, name, vehicleNo, vehicleType, type, password };
      }
  
      const response = await fetch(`http://${ip}:5000/auth/register`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(requestBody), // Pass requestBody directly
      });
      
  
      if (!response.ok) {
        const errorData = await response.json();
        throw new Error(errorData);
      }
  
      const data = await response.json();

      await AsyncStorage.setItem('token', data.jwtToken);
      setIsAuthenticated(true)

      // Handle successful registration
      navigation.navigate('Home');
      
      Toast.show({
        type: 'success',
        text1: 'Registration Successful',
        text2: 'Welcome! Your account has been successfully created.',
        visibilityTime: 3000,
      });

    } catch (error) {
      // Alert.alert('Signup Error', error.message);
      Toast.show({
        type: 'error',
        text1: 'Signup Error',
        text2:error.message
      });
    }
  };
  
  const handleSwitch = (value) => {
    setIsForeigner(value);
    if (!value) { // If user is Bhutanese
        setName(''); 
        setEmailError('');
        setEmailOrPhoneNo('');
        setPassword('');
        setConfirmPassword('');
        setVehicleNo(''); 
        setVehicleType(''); 
    } else { // If user is Foreigner
        setName('');
        setPhoneError('');
        setEmailOrPhoneNo(''); 
        setPassword(''); 
        setConfirmPassword(''); 
        setVehicleNo(''); 
        setVehicleType(''); 
    }
  };
  const validateSignup = () => {
    if (!name.trim()) {
      setNameError('Please enter your name');
      return;
    }
    if (!/^[a-zA-Z ]+$/.test(name.trim())) {
      setNameError('Name should contain only letters');
      return;
    }
    // Clear previous errors
    setNameError('');

    // Phone number validation for Bhutanese
    if (!isForeigner) {
      if (!emailOrPhoneNo.trim()) {
        setPhoneError('Please enter your phone number');
        return;
      }
      if (!/^\d{8}$/.test(emailOrPhoneNo.trim())) {
        setPhoneError('Phone number should be 8 digits');
        return;
      }
      if (!/^((17)|(77))[0-9]{6}$/.test(emailOrPhoneNo.trim())) {
        setPhoneError('Phone number should begin with 17 or 77');
        return;
      }
      // Clear previous errors
      setPhoneError('');
    }

    // Email validation for Foreigners
    if (isForeigner) {
      if (!emailOrPhoneNo.trim()) {
        setEmailError('Please enter your email');
        return;
      }
      if (!/\S+@\S+\.\S+/.test(emailOrPhoneNo.trim())) {
        setEmailError('Please enter a valid email address');
        return;
      }
      // Clear previous errors
      setEmailError('');
    }
    // Vehicle number validation
    if (!vehicleNo.trim()) {
      setVehicleNoError('Please enter your vehicle number');
      return;
    }
    if (!/^(BP|BT)-\d-[a-zA-Z]\d{4}$/.test(vehicleNo.trim())) {
      setVehicleNoError('Vehicle number should be like BP/BT-1-A2345');
      return;
    }
    // Clear previous errors
    setVehicleNoError('');
    
    if (!vehicleType || !['light', 'twoWheeler'].includes(vehicleType)) {
      setVehicleTypeError('Please select a valid vehicle type');
      return;
    }
    // Clear previous errors
    setVehicleTypeError('');

    // Password validation
    if (!password.trim()) {
      setPasswordError('Please enter a password');
      return;
    }
    if (password.trim().length < 6) {
      setPasswordError('Password should have at least 6 characters');
      return;
    }
    // Clear previous errors
    setPasswordError('');

    // Confirm password validation
    if (!confirmPassword.trim()) {
      setConfirmPasswordError('Please confirm your password');
      return;
    }
    if (password.trim() !== confirmPassword.trim()) {
      setConfirmPasswordError('Passwords do not match');
      return;
    }
    // Clear previous errors
    setConfirmPasswordError('');

     // If all validations pass, return true
     return true;
  }

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="light-content" backgroundColor="#1A1B18" />
        <View style={styles.headerContainer}>
          <TouchableOpacity style={styles.iconContainer}>
            <Icon name="arrow-left" size={20} color="#CBE07B" />
          </TouchableOpacity>
          <Text style={styles.register}>Register</Text>
          <Text style={styles.account}>Create your new account</Text>
        </View>
        <View style={styles.switchTextContainer}>
            <CustomSwitch value={isForeigner} onValueChange={handleSwitch} />
            <Text style={styles.switchText}>{isForeigner ? 'Foreigner' : 'Bhutanese'}</Text>
        </View>
        <ScrollView contentContainerStyle={styles.scrollViewContent}>        
          <View style={styles.inputContainer}>
            <TextInput
              style={styles.input}
              placeholder="Full Name"
              placeholderTextColor="rgba(255, 255, 255, 0.5)" 
              value={name}
              onChangeText={text => setName(text)}
            />
            <View style={[styles.inputIcon, { left: 40 }]}>
              <Icon name="user" size={20} color="rgba(255, 255, 255, 0.5)" />
            </View>
          </View>

          {nameError ? <Text style={styles.errorText}>{nameError}</Text> : null}

          {isForeigner && (
            <View style={styles.inputContainer}>
              <TextInput
                style={styles.input}
                placeholder="Email Address"
                placeholderTextColor="rgba(255, 255, 255, 0.5)"
                keyboardType="email-address"
                value={emailOrPhoneNo}
                onChangeText={text => setEmailOrPhoneNo(text)}
              />
              <View style={[styles.inputIcon, { left: 40 }]}>
                <Icon name="envelope" size={20} color="rgba(255, 255, 255, 0.5)" />
              </View>
            </View>
          )}
          {emailError && (
            <View>
              <Text style={styles.errorText}>{emailError}</Text>
            </View>
          )}  
          {!isForeigner && (
            <View style={styles.inputContainer}>
              <TextInput
                style={styles.input}
                placeholder="Phone"
                placeholderTextColor="rgba(255, 255, 255, 0.5)" 
                value={emailOrPhoneNo}
                onChangeText={text => setEmailOrPhoneNo(text)}
              />
              <View style={[styles.inputIcon, { left: 40 }]}>
                <Icon name="phone" size={20} color="rgba(255, 255, 255, 0.5)" />
              </View>
            </View>
          )}
          {phoneError ? <Text style={styles.errorText}>{phoneError}</Text> : null}

          <View style={styles.inputContainer}>
            <TextInput
              style={styles.input}
              placeholder="Vehicle Number"
              placeholderTextColor="rgba(255, 255, 255, 0.5)" 
              value={vehicleNo}
              onChangeText={text => setVehicleNo(text)}
            />
            <View style={[styles.inputIcon, { left: 40 }]}>
              <Icon name="car" size={20} color="rgba(255, 255, 255, 0.5)" />
            </View>
          </View>

          {vehicleNoError && (
            <View>
              <Text style={styles.errorText}>{vehicleNoError}</Text>
            </View>
          )}

          <View style={styles.inputContainer}>
            <Icon name="gear" size={20} color="rgba(255, 255, 255, 0.5)" style={[styles.inputIcon, { left: 40 }]} />
            <View style={[styles.input, styles.pickerContainer]}>
              <Picker
                selectedValue={vehicleType}
                style={styles.picker}
                onValueChange={(itemValue, itemIndex) =>
                  setVehicleType(itemValue)
                }
                // dropdownIconColor="rgba(255, 255, 255, 0.5)"
                // dropdownIconContent={<MaterialIcons name="keyboard-arrow-down" size={24} color="#FFFFFF" />} 
              >
                <Picker.Item label="Vehicle Type" value="" color="#000000" />
                <Picker.Item label="Light Vehicle" value="light" color="#000000" />
                <Picker.Item label="Two Wheelers" value="twoWheeler" color="#000000" />
              </Picker>
              <MaterialIcons name="keyboard-arrow-down" size={24} color="rgba(255, 255, 255, 0.5)" style={styles.dropdownIcon} />
            </View>
          </View>
          
          {vehicleTypeError && (
            <View>
              <Text style={styles.errorText}>{vehicleTypeError}</Text>
            </View>
          )}

          <View style={styles.inputContainer}>
            <TextInput
              style={[styles.input, { height: 49 }]}
              placeholder="Password"
              placeholderTextColor="rgba(255, 255, 255, 0.5)" 
              secureTextEntry={!showPassword}
              value={password}
              onChangeText={text => setPassword(text)}
            />
            <TouchableOpacity
              style={styles.eyeIcon}
              onPress={() => setShowPassword(!showPassword)}
            >
              <MaterialIcons
                name={showPassword ? 'visibility' : 'visibility-off'}
                size={20}
                color="rgba(255, 255, 255, 0.5)"
              />
            </TouchableOpacity>
            <View style={[styles.inputIcon, { left: 40 }]}>
              <Icon name="lock" size={20} color="rgba(255, 255, 255, 0.5)" />
            </View>
          </View>

          {passwordError && (
            <View>
              <Text style={styles.errorText}>{passwordError}</Text>
            </View>
          )}

          <View style={styles.inputContainer}>
            <TextInput
              style={[styles.input, { height: 49 }]}
              placeholder="Confirm Password"
              placeholderTextColor="rgba(255, 255, 255, 0.5)" 
              secureTextEntry={!showConfirmPassword}
              value={confirmPassword}
              onChangeText={text => setConfirmPassword(text)}
            />
            <TouchableOpacity
              style={styles.eyeIcon}
              onPress={() => setShowConfirmPassword(!showConfirmPassword)}
            >
              <MaterialIcons
                name={showConfirmPassword ? 'visibility' : 'visibility-off'}
                size={20}
                color="rgba(255, 255, 255, 0.5)"
              />
            </TouchableOpacity>
            <View style={[styles.inputIcon, { left: 40 }]}>
              <Icon name="lock" size={20} color="rgba(255, 255, 255, 0.5)" />
            </View>
          </View>
          {confirmPasswordError && (
            <View>
              <Text style={styles.errorText}>{confirmPasswordError}</Text>
            </View>
          )}
          <TouchableOpacity
            style={styles.buttonContainer}
            onPress={handleSignup}
          >
            <Text style={styles.buttonText}>Sign Up</Text>
          </TouchableOpacity>
          <View style={styles.loginContainer}>
            <Text style={styles.loginText}>Already have an account? </Text>
            <TouchableOpacity
             onPress={()=>navigation.navigate('Login')}>
              <Text style={styles.highlight}>Login</Text>
            </TouchableOpacity>
          </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1A1B18',
  },
  scrollViewContent: {
    flexGrow: 1,
    justifyContent: 'center',
    paddingHorizontal: 20,
  },
  headerContainer: {
    alignItems: 'center',
    marginTop: 5,
    flexGrow: 1,
    justifyContent: 'center',
    paddingHorizontal: 20,
  },
  iconContainer: {
    position: 'absolute',
    top: '1%',
    left: '7%', 
    zIndex: 1,
  },
  register: {
    color: '#CBE877',
    fontSize: 30,
    fontFamily: 'serif',
    textAlign: 'center',
    marginBottom: 7,
  },
  account: {
    color: '#FFFFFF',
    fontSize: 15,
    fontFamily: 'serif',
    textAlign: 'center',
    marginBottom: 5,
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
  },
  inputIcon: {
    position: 'absolute',
    left: 40, 
    zIndex: 1,
  },
  input: {
    height: 49, 
    borderColor: '#242422',
    borderWidth: 1,
    paddingLeft: 70, 
    backgroundColor: '#242422',
    color: '#FFFFFF',
    flex: 1,
  },
  pickerContainer: {
    flex: 1,
    backgroundColor: '#242422',
    paddingLeft: 10, 
    borderRadius: 5,
    flexDirection: 'row',
    alignItems: 'center',
  },
  picker: {
    flex: 1,
    color: 'rgba(255, 255, 255, 0.5)',
    left:'90%'
  },
  dropdownIcon: {
    position: 'absolute',
    right: 10,
  },
  switchTextContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 0,
    marginLeft: 0, 
    flexGrow: 1,
    paddingHorizontal: 20,
  }, 
  switchText: {
    color: '#FFFFFF',
    marginLeft: 10,
  },
  switchContainer: {
    borderWidth: 1,
    borderColor: '#CBE877',
    borderRadius: 15,
    padding: 2,
    width: 50,
    height: 30,
    justifyContent: 'center',
  },
  switch: {
    width: 20,
    height: 20,
    borderRadius: 10,
    backgroundColor: '#CBE877',
  },
  switchActive: {
    transform: [{ translateX: 25 }],
  },
  buttonContainer: {
    width: '100%',
    backgroundColor: '#CBE877',
    paddingVertical: 10,
    borderRadius: 20,
    marginBottom: 10,
  },
  buttonText: {
    color: '#1A1B18',
    fontSize: 18,
    textAlign: 'center',
    fontWeight: '600',
  },
  loginContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  loginText: {
    color: '#808080',
  },
  highlight: {
    color: '#CBE877',
  },
  eyeIcon: {
    position: 'absolute',
    right: '5%', 
    zIndex: 1,
  },
  errorText: {
    color: '#FF0000',
    marginTop: -15,
    marginLeft: 60,
    marginBottom:20,
  },
});

export default SignupPage;
