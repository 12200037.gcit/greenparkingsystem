import React, { useEffect, useState, useRef } from 'react';
import { View, Text, Alert, StyleSheet, TouchableOpacity, Animated, Modal, PanResponder, Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Toast from 'react-native-toast-message';
import MyCurrentParkingScreen from '../stack/MyCurrentParkingStatus';
import {ip} from '../../utils/config'

const HomeScreen = ({ navigation, route }) => {
  const [vehicle, setVehicle] = useState([]);
  const { setIsAuthenticated } = route.params;
  const [area, setArea] = useState([]);
  const [selectedMarker, setSelectedMarker] = useState([])
  const [selectedCarNumber, setSelectedCarNumber] = useState('');
  const [showAdditionalCarNumbers, setShowAdditionalCarNumbers] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const additionalCarNumbersHeight = useRef(new Animated.Value(0)).current;
  const additionalCarNumbersOpacity = useRef(new Animated.Value(0)).current;
  const [showParkingContent, setShowParkingContent] = useState(false);
  const [swipeIcon, setSwipeIcon] = useState('angle-double-up'); 
  const [status, setStatus] = useState([]);

  const [todo, setTodo] = useState(false);

  //getting vehicle
  const getVehicle = async () => {
    try {
      const token = await AsyncStorage.getItem('token');

      const response = await fetch(`http://${ip}:5000/user/vehicle/`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'token': token,
        },
      });

      if (!response.ok) {
        const errorData = await response.json();
        throw new Error(errorData); 
      }

      const data = await response.json();
      setVehicle(data);
      setSelectedCarNumber(data[0].vehicleno);

    } catch (error) {
      console.error(error.message);
      Alert.alert('Home Error', error.message);
    }
  };

  //get parking status
  const getStatus = async () => {
    try {
      const token = await AsyncStorage.getItem('token');
      console.log(token)
      const response = await fetch(`http://${ip}:5000/user/status/`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'token': token,
        },
      });

      if (!response.ok) {
        const errorData = await response.json();
        throw new Error(errorData);
      }

      const data = await response.json();
      setStatus(data);

    } catch (error) {
      Alert.alert('Current Parking Status Error', error.message);
    }
  };

  //get areas
  const getArea = async () => {
    try {
      const token = await AsyncStorage.getItem('token');
      
      if (!token){
        setIsAuthenticated(false)
      }

      const response = await fetch(`http://${ip}:5000/user/area/`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'token': token,
        },
      });

      if (!response.ok) {
        const errorData = await response.json();
        throw new Error(errorData);
      }

      const data = await response.json();
      setArea(data);
    } catch (error) {
      console.error(error.message);
      Alert.alert('Home Error', error.message);
    }
  };

  //for car number toggle
  const toggleAdditionalCarNumbers = () => {
    setShowAdditionalCarNumbers(!showAdditionalCarNumbers);
    if (!showAdditionalCarNumbers) {
      Animated.timing(additionalCarNumbersHeight, {
        toValue: 90,
        duration: 300,
        useNativeDriver: false,
      }).start();
      Animated.timing(additionalCarNumbersOpacity, {
        toValue: 1,
        duration: 300,
        useNativeDriver: false,
      }).start();
    } else {
      Animated.timing(additionalCarNumbersHeight, {
        toValue: 0,
        duration: 300,
        useNativeDriver: false,
      }).start();
      Animated.timing(additionalCarNumbersOpacity, {
        toValue: 0,
        duration: 300,
        useNativeDriver: false,
      }).start();
    }
  };
  //choose parking arae
  const toggleModal = (marker) => {
    setSelectedMarker(marker);
    setModalVisible(!modalVisible);
    
  };
  
  //select car number from multiple vehicle
  const selectCarNumber = (carNumber) => {
    setSelectedCarNumber(carNumber);
    toggleAdditionalCarNumbers();
  };
  
  //park car
  const handlePark = async () => {
    try {
      const token = await AsyncStorage.getItem('token');

      const name = selectedMarker.area;
      const a_id = selectedMarker.a_id;
      const vehicleNo = selectedCarNumber;

      const body = { name, a_id, vehicleNo };
      const response = await fetch(`http://${ip}:5000/user/parking`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'token': token
        },
        body: JSON.stringify(body),
      });
      
      if (!response.ok) {
        const errorData = await response.json();
        throw new Error(errorData); 
      }
        
      setModalVisible(false);
      setTodo(true)
      navigation.navigate('Home')
      Toast.show({
        type: 'success',
        text1: 'Parked Successfully',
        visibilityTime: 3000,
      });

    } catch (error) {
      Alert.alert('Parking Error', error.message);
    }
  };

  //fetch data
  useEffect(() => {
    getArea();
    getVehicle();
    getStatus();
    setTodo(false);
  }, [todo]);

  
  //bottom up slider
  const panResponder = useRef(
    PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderMove: (_, gestureState) => {
        if (gestureState.dy < -50) {
          setShowParkingContent(true);
          setSwipeIcon('angle-double-down'); 
        } else if (gestureState.dy > 50) {
          setShowParkingContent(false);
          setSwipeIcon('angle-double-up'); 
        }
      },
      onPanResponderRelease: () => {
      },
    })
  ).current;

  return (
    <View style={styles.container}>
      <Image
        source={require('../../../assets/Logo.png')}
        style={styles.logo}
        resizeMode="contain"
      />
      <View style={styles.textContainer}>

        {/* My car component */}
        <TouchableOpacity onPress={toggleAdditionalCarNumbers}>
          <View style={styles.presentCar}>
            <Image
              source={require('../../../assets/Car.png')}
              style={styles.car}
              resizeMode="contain"
            />
            <View style={styles.overallview}>
              <Text style={styles.text}>My Car</Text>
              <Text style={styles.title}>{selectedCarNumber}</Text>
            </View>
          </View>
        </TouchableOpacity>

        <Animated.View
          style={[
            styles.additionalCarNumbersContainer,
            { height: additionalCarNumbersHeight, opacity: additionalCarNumbersOpacity },
          ]}
        >
          {vehicle.map((carNumber) => (
          <TouchableOpacity key={carNumber.v_id} onPress={() => selectCarNumber(carNumber.vehicleno)}>
            <Text style={styles.number}>{carNumber.vehicleno}</Text>
          </TouchableOpacity>
        ))}
        </Animated.View>
      </View>

      {area.map((marker, index) => (
        <TouchableOpacity onPress={() => toggleModal(marker)} key={index}>
          <React.Fragment>
            <Text style={{ color: "white" , marginTop:30}}>{marker.area}    {marker.availableslots}</Text>
            <Icon name="map-marker" size={30} color="#CBE877" />
          </React.Fragment>
        </TouchableOpacity>
      ))}

      {!status ? (
        <div>Loading...</div>
      ) : (
        status.length >= 1 && <MyCurrentParkingScreen data={status[0]} setTodo={setTodo} />
      )}

      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.modalContainer}>
          <View style={styles.modalContent} {...panResponder.panHandlers}>
            <Image
              source={{ uri: selectedMarker.img}}
              // source={require('../../../assets/Parking.jpg')}
              style={styles.modalImage}
              resizeMode="cover"
              alt='no image'
            />
            <TouchableOpacity style={styles.swipeUpIconContainer}>
              <Icon name={swipeIcon} size={30} color="white" />
            </TouchableOpacity>
            <View>
              <Text style={styles.modalDescription}>
                {selectedMarker.area}
              </Text>
              <Text style={styles.Description}>
                {selectedMarker.descrip}
              </Text>
            </View>
              <View style={styles.all}>
                <View style={styles.secondView}>
                  <Text style={styles.lot}>{selectedMarker.totalslot}</Text>
                  <Text style={styles.desc}>Total(Lots)</Text>
                </View>
                <View style={styles.secondView}>
                  <Text style={styles.lot}>{selectedMarker.availableslots}</Text>
                  <Text style={styles.desc}>Available(Lots)</Text>
                </View>
                <View style={styles.secondView}>
                  <Text style={styles.lot}><Text style={styles.nu}>Nu.</Text>{selectedMarker.light_price}</Text>
                  <Text style={styles.desc}>light (/30 mins)</Text>
                </View>
                <View style={styles.secondView}>
                  <Text style={styles.lot}><Text style={styles.nu}>Nu.</Text>{selectedMarker.twowheeler_price}</Text>
                  <Text style={styles.desc}>twoWheel(/30m)</Text>
                </View>
              </View>

            {showParkingContent && (
              <View style={styles.secondpart}>
                <Text style={styles.make}>Make Your Parking</Text>
                <TouchableOpacity style={styles.parkButton} onPress={handlePark}>
                  <Text style={styles.parkButtonText}>Park</Text>
                </TouchableOpacity>
              </View>
            )}
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1A1B18',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
  },
  logo: {
    width: '10%',
    height: '10%',
    position: 'absolute',
    top: '1%',
    left: 20,
  },
  textContainer: {
    position: 'absolute',
    top: '1%',
    right: '3%',
  },
  presentCar: {
    backgroundColor: 'rgba(36, 36, 34, 1)',
    padding: 15,
    borderColor: 'rgba(255, 255, 255, 0.2)',
    borderWidth: 1,
    borderRadius: 9,
    flexDirection: 'row',
    alignItems: 'center',
  },
  text: {
    color: 'rgba(255, 255, 255, 0.36)',
    fontSize: 13,
    fontWeight: '500',
    marginLeft: 10,
  },
  number: {
    color: 'rgba(255, 255, 255, 0.36)',
    fontSize: 17,
    fontWeight: '400',
    padding: 5,
  },
  horizontalLine: {
    width: '60%',
    height: 1,
    backgroundColor: 'rgba(255, 255, 255, 0.2)',
  },  
  title: {
    color: 'white',
    fontSize: 17,
    fontWeight: '400',
    marginLeft: 10,
  },
  car: {
    width: 40,
    height: 40,
    marginRight: 10,
  },
  additionalCarNumbersContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(36, 36, 34, 1)',
    borderBottomStartRadius: 13,
    borderBottomEndRadius: 13,
    // padding:34
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: '#1A1B18',
  },
  modalContent: {
    backgroundColor: '#1A1B18',
    width: '100%',
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
    alignItems: 'center',
  },
  modalImage: {
    width: '100%',
    height: 150,
    marginBottom: 10,
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
  },
  modalDescription: {
    fontSize: 16,
    marginTop: '10%',
    fontWeight: '500',
    marginHorizontal: '7%',
    color: 'rgba(255, 255, 255, 0.7)'
  },
  Description: {
    fontSize: 14,
    marginTop: '5%',
    fontWeight: '300',
    marginHorizontal: '7%',
    color: 'rgba(255, 255, 255, 0.7)'
  },
  all: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: '7%',
    marginBottom: '7%',
    marginHorizontal: '10%'
  },
  secondView: {
    flex: 1,
    alignItems: 'center'
  },
  lot: {
    color: 'white',
    fontSize: 20,
    fontWeight: '400',
  },
  desc: {
    color: 'rgba(255, 255, 255, 0.5)',
    fontWeight: '400',
    fontSize: 10,
  },
  nu: {
    color: 'rgba(255, 255, 255, 0.5)',
    fontSize: 10
  },
  secondpart: {
    alignSelf: 'stretch',
    marginHorizontal: '10%'
  },
  make: {
    color: 'rgba(255, 255, 255, 0.7)',
    fontWeight: '500',
    fontSize: 15,
    textAlign: 'left'
  },
  picker: {
    width: 300,
    padding: 10,
    marginTop: '4%',
    backgroundColor: '#242422',
    color: 'white',
  },
  parkButton: {
    backgroundColor: '#CBE877',
    width: 300,
    paddingVertical: 13,
    borderRadius: 30,
    marginTop: 15,
    marginBottom: 10,
  },
  parkButtonText: {
    color: '#0A1C1F',
    fontSize: 17,
    fontWeight: '600',
    textAlign: 'center'
  },
  swipeUpIconContainer: {
    position: 'absolute',
    top: 10,
    alignSelf: 'center',
  },
});

export default HomeScreen;
