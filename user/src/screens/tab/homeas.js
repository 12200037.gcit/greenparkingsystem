import React from 'react';
import { View, Text, StyleSheet, SafeAreaView, StatusBar, TouchableOpacity, Modal, TextInput} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Picker } from '@react-native-picker/picker';
import { ScrollView } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';

const ProfileScreen = () => {

  const [modalVisible, setModalVisible] = React.useState(false);
  const [vehicleNumber, setVehicleNumber] = React.useState('');
  const [selectedVehicleType, setSelectedVehicleType] = React.useState('Light Vehicle');
  const [editModalVisible, setEditModalVisible] = React.useState(false);
  const [fullName, setFullName] = React.useState('');
  const [oldPassword, setOldPassword] = React.useState('');
  const [newPassword, setNewPassword] = React.useState('');
  const [confirmPassword, setConfirmPassword] = React.useState('');
  const [logoutConfirmationVisible, setLogoutConfirmationVisible] = React.useState(false);
  const [deleteConfirmationVisible, setDeleteConfirmationVisible] = React.useState(false);
  const [vehicleToDelete, setVehicleToDelete] = React.useState({});
  const navigation = useNavigation();

  const handleSubmit = () => {
    console.log('Vehicle Number:', vehicleNumber);
    console.log('Vehicle Type:', selectedVehicleType);
    setModalVisible(false);
  };

  const handleCloseModal = () => {
    setModalVisible(false);
  };

  const handleEditSubmit = () => {
    console.log('Profile edited');
    setEditModalVisible(false);
  };

  const handleCloseEditModal = () => {
    setEditModalVisible(false);
  };

  const handleSignOutPress = () => {
    setLogoutConfirmationVisible(true);
  };

  const handleLogoutConfirmation = (confirmed) => {
    if (confirmed) {
      navigation.navigate('Login'); // Replace 'Login' with the name of your login screen
    }
    setLogoutConfirmationVisible(false);
  };
  const handleDeleteVehicleConfirmation = (vehicleNumber, vehicleType) => {
    setVehicleToDelete({ vehicleNumber, vehicleType });
    setDeleteConfirmationVisible(true);
  };
  
  const handleDeleteVehicle = () => {
    // Perform deletion logic here
    console.log('Vehicle deleted:', vehicleToDelete);
    setDeleteConfirmationVisible(false);
  };
  
  const handleCloseDeleteConfirmation = () => {
    setDeleteConfirmationVisible(false);
  };
  

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="light-content" backgroundColor="#1A1B18" />
      <View style={styles.topp}>      
        <Text style={styles.title}>My Profile</Text>
          <TouchableOpacity style={styles.profileArrow} onPress={handleSignOutPress}>
            <Icon name="sign-out" size={20} color="white" />
          </TouchableOpacity>
      </View>
      <ScrollView>
        <View style={styles.overall}>
          <Text style={styles.label1}>My Account Info</Text>
          <View style={styles.editContainer}>
            <TouchableOpacity onPress={() => setEditModalVisible(true)}>
              <Text style={styles.edit}>Edit</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.name}>
          <Text style={styles.nametext}>Kuenzang Tshering</Text>
        </View>
        <View style={styles.name}>
          <Text style={styles.nametext}>17890882</Text>
        </View>
        {/* <View style={styles.name}>
          <Text style={styles.nametext}>****</Text>
        </View> */}
        <TouchableOpacity style={styles.button}>
        <Text style={styles.buttonText}>Change Password</Text>
        </TouchableOpacity>
        <View style={styles.overallvehicle}>
          <Text style={styles.label1}>My Vehicle Info</Text>
          <View style={styles.editContainer}>
            <TouchableOpacity onPress={() => setModalVisible(true)}>
              <Text style={styles.edit}>Add Vehicle</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.horizon}>
          <View style={styles.horizontalLineFirstHalf} />
          <Text style={styles.vehicle}>Vehicle 1</Text>
          <View style={styles.horizontalLineSecondHalf} />
        </View>
        <TouchableOpacity style={styles.delete} onPress={() => handleDeleteVehicleConfirmation('BP-1-A1001', 'Light Vehicle')}>
          <Icon name="trash" size={20} color="#929291" />
        </TouchableOpacity>
        <View style={styles.name}>
          <Text style={styles.nametext}>BP-1-A1001</Text>
        </View>
        <View style={styles.name1}>
          <Text style={styles.nametext}>Light Vehicle</Text>
        </View>
        <View style={styles.horizon1}>
          <View style={styles.horizontalLineFirstHalf} />
          <Text style={styles.vehicle}>Vehicle 2</Text>
          <View style={styles.horizontalLineSecondHalf} />
        </View>
        <View style={styles.name}>
          <Text style={styles.nametext}>BP-1-A3210</Text>
        </View>
        <View style={styles.name1}>
          <Text style={styles.nametext}>Two Wheelers</Text>
        </View>
      </ScrollView>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(false);
        }}
      >
        <View style={styles.modalBackground}>
          <View style={styles.modalView}>
            <TouchableOpacity style={styles.closeButton} onPress={handleCloseModal}>
              <Icon name="times" size={20} color="rgba(255, 255, 255, 0.5)" />
            </TouchableOpacity>
            <Text style={styles.modalTitle}>Add new vehicle</Text>
            <TextInput
              style={styles.input}
              placeholder="Vehicle Number"
              placeholderTextColor="rgba(255, 255, 255, 0.5)"
              onChangeText={setVehicleNumber}
              value={vehicleNumber}
            />
            <View style={styles.pick}>
              <Picker
                selectedValue={selectedVehicleType}
                style={styles.picker}
                onValueChange={(itemValue, itemIndex) =>
                  setSelectedVehicleType(itemValue)
                }
                dropdownIconColor="rgba(255, 255, 255, 0.5)"
              >
                <Picker.Item label="Light Vehicle" value="Light Vehicle" />
                <Picker.Item label="Two Wheelers" value="Two Wheelers" />
              </Picker>
            </View>

            <TouchableOpacity
              style={styles.addButton}
              onPress={handleSubmit}
            >
              <Text style={styles.addButtonText}>Add</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
      <Modal
        animationType="slide"
        transparent={true}
        visible={editModalVisible}
        onRequestClose={() => {
          setEditModalVisible(false);
        }}
      >
        <View style={styles.modalBackground}>
          <View style={styles.modalView}>
            <TouchableOpacity style={styles.closeButton} onPress={handleCloseEditModal}>
              <Icon name="times" size={20} color="rgba(255, 255, 255, 0.5)" />
            </TouchableOpacity>
            <Text style={styles.modalTitle}>My Account Info</Text>
            <ScrollView>
            <TextInput
              style={styles.input}
              placeholder="Full Name"
              placeholderTextColor="rgba(255, 255, 255, 0.5)"
              onChangeText={setFullName}
              value={fullName}
            />
            <TextInput
              style={styles.input}
              placeholder="Old Password"
              placeholderTextColor="rgba(255, 255, 255, 0.5)"
              secureTextEntry={true}
              onChangeText={setOldPassword}
              value={oldPassword}
            />
            
            <TextInput
              style={styles.input}
              placeholder="New Password"
              placeholderTextColor="rgba(255, 255, 255, 0.5)"
              secureTextEntry={true}
              onChangeText={setNewPassword}
              value={newPassword}
            />
            <TextInput
              style={styles.input}
              placeholder="Confirm Password"
              placeholderTextColor="rgba(255, 255, 255, 0.5)"
              secureTextEntry={true}
              onChangeText={setConfirmPassword}
              value={confirmPassword}
            />
            <TouchableOpacity
              style={styles.addButton}
              onPress={handleEditSubmit}
            >
              <Text style={styles.addButtonText}>Update</Text>
            </TouchableOpacity>
            </ScrollView>
          </View>
        </View>
      </Modal>
      <Modal
        animationType="slide"
        transparent={true}
        visible={logoutConfirmationVisible}
        onRequestClose={() => setLogoutConfirmationVisible(false)}
      >
        <View style={styles.modalBackground}>
          <View style={styles.modalView}>
            <Text style={styles.modalTitle}>Log out of your account?</Text>
            <View style={styles.buttonContainer}>
              <TouchableOpacity
                style={styles.modalButton}
                onPress={() => handleLogoutConfirmation(false)} // If yes is pressed
              >
                <Text style={styles.modalButtonText}>CANCEL</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.modalButton}
                onPress={() => handleLogoutConfirmation(true)} // If no is pressed
              >
                <Text style={[styles.modalButtonText, {color:'red'}]}>LOG OUT</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
      <Modal
  animationType="slide"
  transparent={true}
  visible={deleteConfirmationVisible}
  onRequestClose={() => setDeleteConfirmationVisible(false)}
>
  <View style={styles.modalBackground}>
    <View style={styles.modalView}>
      <TouchableOpacity style={styles.closeButton} onPress={handleCloseDeleteConfirmation}>
        <Icon name="times" size={20} color="rgba(255, 255, 255, 0.5)" />
      </TouchableOpacity>
      <Text style={styles.modalTitle}>Are you sure you want to unregister the vehicle?</Text>
      <View style={styles.buttonContainer}>
        <TouchableOpacity
          style={styles.modalButton}
          onPress={handleCloseDeleteConfirmation} // If cancel is pressed
        >
          <Text style={styles.modalButtonText}>CANCEL</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.modalButton}
          onPress={handleDeleteVehicle} // If delete is pressed
        >
          <Text style={[styles.modalButtonText, { color: 'red' }]}>DELETE</Text>
        </TouchableOpacity>
      </View>
    </View>
  </View>
</Modal>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1A1B18', 
  },
  overall: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 25, 
    marginTop: '7%', 
  },
  overallvehicle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 25, 
    marginTop: '10%', 
  },
  topp: {
    flexDirection: 'row',
    marginTop: '3%'
  },
  profileArrow: {
    position: 'absolute',
    top: '15%',
    right:'7%',
    zIndex: 1,
  },
  title: {
    fontSize: 21,
    fontWeight: '500',
    color: 'white',
    flexDirection: 'row',
    left: '7%',
  },
  label1: {
    fontSize: 15,
    fontWeight: '500',
    color: '#7C7C7C',
    textAlign: 'left',
  },
  editContainer: {
    flex: 1,
    alignItems: 'flex-end', 
  },
  edit: {
    fontSize: 15,
    fontWeight: '500',
    color: '#C5E274',
    textAlign: 'right',
  },
  name:{
    backgroundColor:'#242422',
    marginHorizontal: "7%",
    marginTop: 15,
  },
  button: {
    backgroundColor: '#CBE877',
    paddingVertical: 15,
    borderRadius: 40,
    marginBottom: 20,
    marginHorizontal: "7%",
    marginTop: 15,
  },
  buttonText: {
    color: '#1A1B18',
    fontSize: 16,
    textAlign: 'center',
    fontWeight: '600',
  },
  name1:{
    backgroundColor:'#242422',
    marginHorizontal: "7%",
    marginTop: 15,
    marginBottom:'3%'
  },
  nametext:{
    color:'#929291',
    padding:'5%',
    marginLeft:'5%',
    fontSize:15
  },
  horizontalLineFirstHalf: {
    borderBottomColor: '#7C7C7C',
    borderBottomWidth: 1,
    flex: 0.5, 
    marginVertical: 20,
    marginLeft:"13%" 
  },
  horizontalLineSecondHalf: {
    borderBottomColor: '#7C7C7C',
    borderBottomWidth: 1,
    flex: 0.5, 
    marginVertical: 20,
    marginRight:"13%"
  },
  horizon: {
    flexDirection: 'row',
    alignItems:'center'
  },
  horizon1: {
    flexDirection: 'row',
    alignItems:'center',
  },
  delete:{
    position: 'absolute',
    top: '15%',
    right:'7%',
    zIndex: 1,
  },
  vehicle: {
    color: '#7C7C7C',
    paddingHorizontal: 10,
  },
  modalBackground: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    justifyContent: 'center',
  },
  modalView: {
    backgroundColor: '#242422',
    borderRadius: 20,
    padding: 35,
    marginHorizontal:'5%',
    elevation: 5,
  },
  modalTitle: {
    fontSize: 17,
    marginBottom: 25,
    fontWeight:'400',
    color: 'rgba(255, 255, 255, 0.7)',
  },
  input: {
    width: '100%',
    padding: 10,
    marginBottom: 20,
    borderColor:'rgba(255, 255, 255, 0.1)',
    borderWidth:1,
    borderRadius: 10, 
    color:'white',
    fontSize:15,    
  },
  picker: {
    color: 'rgba(255, 255, 255, 0.5)',
    fontSize: 15,
    width: '100%',
  },
  pick:{   
    marginBottom: 20,
    borderColor: 'rgba(255, 255, 255, 0.1)', 
    borderWidth: 1, 
    borderRadius: 10, 
  },
  addButton: {
    backgroundColor: '#C5E274',
    padding: 13,
    borderRadius: 25,
    alignItems: 'center',
    width: '100%',
    marginHorizontal:0,
    paddingHorizontal:0,
  },
  addButtonText: { 
    color: '#0A1C1F',
    fontSize: 16,
    fontWeight: '600',
  },  
  closeButton: {
    position: 'absolute',
    padding:'15%',
    left:'90%',
    zIndex: 1,
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  modalButton: {
    width: '30%',
    alignItems: 'right',
  },
  modalButtonText: {
    color: 'white',
    fontSize: 15,
    fontWeight: '500',
    textAlign:'right'
  },
});

export default ProfileScreen;