import React, { useState, useEffect } from 'react';
import { View, Text, Alert, StyleSheet, TouchableOpacity, SafeAreaView, StatusBar, ScrollView, Modal } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {ip} from '../../utils/config'

const HistoryScreen = () => {
  const navigation = useNavigation();
  const [modalVisible, setModalVisible] = useState(false);

  const [history, setHistory] = useState([]);
  const [marker, setMarker] = useState([])

  //get history
  const getHistory = async () => {
    try {
      const token = await AsyncStorage.getItem('token');
 
      const response = await fetch(`http://${ip}:5000/user/history/`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'token': token,
        },
      });
  
      if (!response.ok) {
        // If response status is not okay, extract and handle the error message
        const errorData = await response.json();
        throw new Error(errorData); // Throw backend error message
      }
  
      const data = await response.json();
  
      setHistory(data);
  
    } catch (error) {
      Alert.alert('History Error', error.message);
    }
  };
  //set marker
  const handleHistoryDetail = (item) => {
    setMarker(item);
    setModalVisible(true);
  };

  const handleCloseModal = () => {
    setModalVisible(false);
  };


  //time convertion
  const convertTo12HourTime = (timeString) => {
    if (!timeString) {
      return 'N/A'; // Or any default value to indicate that the time is not available
    }
  
    const [hours, minutes] = timeString.split(':').map(Number);
    const period = hours >= 12 ? 'PM' : 'AM';
    const displayHours = hours % 12 || 12;
    return `${displayHours}:${minutes < 10 ? '0' : ''}${minutes} ${period}`;
  };

  //date convertion
  const formatDate = (dateString) => {
    const date = new Date(dateString);
    const options = { day: 'numeric', month: 'long', year: 'numeric' };
    return date.toLocaleDateString('en-GB', options);
  };

  //duration convertion
  const formatDuration = (timeString) => {
    if (!timeString) {
      return 'N/A'; // Or any default value to indicate that the time is not available
    }
    // Split the time string into hours, minutes, and seconds
    const [hours, minutes] = timeString.split(':').map(Number);
  
    // Construct the formatted string
    let formattedString = '';
    if (hours > 0) {
      formattedString += `${hours}h `;
    }
    if (minutes > 0 || !formattedString) {
      formattedString += `${minutes}m`;
    }
    return formattedString.trim();
  };


  useEffect(() => {
    getHistory();
  }, []); 

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="light-content" backgroundColor="#1A1B18" />
      <View style={styles.topmost}>
        {/* <TouchableOpacity style={styles.arrow} onPress={handleHistory}>
          <Icon name="arrow-left" size={20} color="#7C7C7C" />
        </TouchableOpacity> */}
        <Text style={styles.title}>My Parking History</Text>
      </View>

      {/* history */}
      <ScrollView contentContainerStyle={styles.historyContainer}>
      {history.length === 0 ? (
        <View style={styles.short}>
          <Text style={styles.cost}>No history available</Text>
        </View>
      ) : (
        history.map((item, index) => (
          <React.Fragment key={index}>
            {/* Convert the ISO string date to 'YYYY-MM-DD' format */}
            <Text style={styles.date}>{formatDate(item.date)}</Text>

            <TouchableOpacity style={styles.short} onPress={() => handleHistoryDetail(item)} key={index}>
              <View style={styles.textContainer}>
                <Text style={styles.placeName}>{item.area}</Text>
                <Text style={styles.time}>{convertTo12HourTime(item.starttime)} - {convertTo12HourTime(item.endtime)}</Text>
              </View>
              <Text style={styles.cost}>NU. {item.totalamount}</Text>
            </TouchableOpacity>
          </React.Fragment>
        ))
      )}

      </ScrollView>

      <Modal
        animationType="fade"
        transparent={true}
        visible={modalVisible}
        onRequestClose={handleCloseModal}
      >
        <View style={styles.modalBackground}>
          <View style={styles.modalView}>
            <TouchableOpacity style={styles.closeButton} onPress={handleCloseModal}>
              <Icon name="times" size={20} color="rgba(255, 255, 255, 0.5)" />
            </TouchableOpacity>

            <Text style={styles.modalTitle}>Parking Details</Text>
            <View style={styles.detailsContainer}>
              <View style={styles.detailItem}>
                <Text style={styles.detailLabel}>Name</Text>
                <Text style={styles.detailValue}>:  {marker.user_name}</Text>
              </View>
              <View style={styles.detailItem}>
                <Text style={styles.detailLabel}>Area</Text>
                <Text style={styles.detailValue}>:  {marker.area}</Text>
              </View>
              <View style={styles.detailItem}>
                <Text style={styles.detailLabel}>Vehicle No</Text>
                <Text style={styles.detailValue}>:  {marker.vehicleno}</Text>
              </View>
              <View style={styles.detailItem}>
                <Text style={styles.detailLabel}>Date</Text>
                <Text style={styles.detailValue}>:  {formatDate(marker.date)}</Text>
              </View>
              <View style={styles.detailItem}>
                <Text style={styles.detailLabel}>Start Time</Text>
                <Text style={styles.detailValue}>:  {convertTo12HourTime(marker.starttime)}</Text>
              </View>
              <View style={styles.detailItem}>
                <Text style={styles.detailLabel}>End Time</Text>
                <Text style={styles.detailValue}>:  {convertTo12HourTime(marker.endtime)}</Text>
              </View>
              <View style={styles.detailItem}>
                <Text style={styles.detailLabel}>Total Time</Text>
                <Text style={styles.detailValue}>:  {formatDuration(marker.duration)}</Text>
              </View>
              <View style={styles.detailItem}>
                <Text style={styles.detailLabel}>Total Amount</Text>
                <Text style={styles.detailValue}>:  Nu. {marker.totalamount}</Text>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1A1B18', 
  },
  topmost:{
    flexDirection:'row',
    marginTop:'3%'
  },
  historyContainer: {
    flexGrow: 1,
  },
  arrow: {
    position: 'absolute',
    top: '15%',
    left: '7%', 
    zIndex: 1,
  },
  title: {
    fontSize: 21,
    fontWeight: '500',
    color: 'white',
    flexDirection:'row',
    left:'15%',
  },
  date: {
    fontSize: 16,
    color: '#7C7C7C',
    top:'3%',
    left: '7%', 
  },  
  short:{
    backgroundColor:'#242422',
    marginTop:'10%',
    borderRadius:15,
    marginHorizontal:'5%',
    padding:'7%',
    flexDirection:'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  shorts:{
    backgroundColor:'#242422',
    marginTop:'5%',
    borderRadius:15,
    marginHorizontal:'5%',
    padding:'7%',
    flexDirection:'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  brief:{
    backgroundColor:'#242422',
    marginTop:'5%',
    borderRadius:15,
    marginHorizontal:'5%',
    padding:'7%',
    flexDirection:'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textContainer: {
    flex: 1,
  },
  placeName: {
    color:'#7C7C7C',
    fontSize:18,
    fontWeight:'500',
    textTransform: 'capitalize'
  },
  time:{
    color:'#7C7C7C',
    top:'10%',
    textAlign:'left'
  },
  cost:{
    color:'#CBE877',
    textAlign:'right',
    flexDirection:'row',
    fontSize:15,
    fontWeight:'500'
  },
  last:{
    marginBottom:'5%'
  },
  modalBackground: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalView: {
    backgroundColor: '#242422',
    borderRadius: 20,
    padding: 35,
    width: '87%',
    alignItems: 'left',
  },
  modalTitle: {
    fontSize: 20,
    marginBottom: 15,
    fontWeight:'500',
    color: 'rgba(255, 255, 255, 0.4)',
    textAlign:'left'
  },
  closeButton: {
    position: 'absolute',
    padding:'27%',
    left:'90%',
    zIndex: 1,
  },
  detailsContainer: {
    marginTop: 10,
  },
  detailItem: {
    flexDirection: 'row',
    marginBottom: 15,
  },
  detailLabel: {
    flex: 1,
    color: '#929291',
    fontSize:15,
    fontWeight: '400',
  },
  detailValue: {
    color: '#929291',
    flex: 1.5,
    fontWeight:'400',
    fontSize:15,
    textAlign:'left',
  },
});

export default HistoryScreen;
