import { View, Alert,Text, StyleSheet, SafeAreaView, StatusBar, TouchableOpacity, Modal, TextInput} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import React, {useEffect, useState} from 'react';
import { Picker } from '@react-native-picker/picker';
import { ScrollView } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Toast from 'react-native-toast-message';
import { useNavigation } from '@react-navigation/native';
import { ip } from '../../utils/config';

const ProfileScreen = ({route}) => {

  const { setIsAuthenticated } = route.params;

  const [modalVisible, setModalVisible] = useState(false);
  const [vehicleNumber, setVehicleNumber] = useState('');
  const [selectedVehicleType, setSelectedVehicleType] = useState('Light Vehicle');
  const [editModalVisible, setEditModalVisible] = useState(false);
  const [oldPassword, setOldPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const navigation = useNavigation();
  const [profile, setProfile] = useState([]);
  const [name, setName] = useState("");
  const [vehicle, setVehicle] = useState([])
  const [logoutConfirmationVisible, setLogoutConfirmationVisible] = useState(false);
  const [deleteConfirmationVisible, setDeleteConfirmationVisible] = useState(false);




  //get profile data
  const getProfile = async () => {
    try {
      const token = await AsyncStorage.getItem('token');

      const response = await fetch(`http://${ip}:5000/user/profile`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'token': token,
        },
      });

      if (!response.ok) {
        const errorData = await response.json();
        throw new Error(errorData);
      }

      const profile = await response.json();
      setProfile(profile);
      setName(profile.user_name);

    } catch (error) {
      console.error(error.message);
      Alert.alert('Profile Error', error.message);
    }
  };

  //get Vehicle
  const getVehicle = async () => {
    try {
      const token = await AsyncStorage.getItem('token');

      const response = await fetch(`http://${ip}:5000/user/vehicle`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'token': token,
        },
      });

      if (!response.ok) {
        const errorData = await response.json();
        throw new Error(errorData);
      }

      const vehicle = await response.json();
      setVehicle(vehicle);
    } catch (error) {
      console.error(error.message);
      Alert.alert('Profile Error', error.message);
    }
  };
  

  useEffect(() => {
    getProfile();
    getVehicle();
  }, []); 

  const handleCloseModal = () => {
    setModalVisible(false);
  };

  //update username
  const handleEditUserName = async () => {
    try {
      const body = { name };
      const token = await AsyncStorage.getItem('token');
  
      const response = await fetch(`http://${ip}:5000/user/username`, {
        method: "PUT",
        headers: {
          'Content-Type': 'application/json',
          'token': token,
        },
        body: JSON.stringify(body)
      });
  
      if (!response.ok) {
        const errorData = await response.json();
        throw new Error(errorData);
      }
  
      // Logic for handling profile edit submission
      console.log('Profile edited');
      // Clear input fields by resetting state variables
      // setName('');
      // Refresh profile data
      getProfile();
      Toast.show({
        type: 'success',
        text1: 'Successfully Changed Username!',
        visibilityTime: 3000, // 3 seconds
      });
      setEditModalVisible(false);

    } catch (error) {
      console.error(error.message);
      Alert.alert('UserName Edit Error', error.message);
    } 
  };

  //update password
  const handleEditPassword = async () => {
    try {
      const body = { newPassword, oldPassword };
      const token = await AsyncStorage.getItem('token');
  
      const response = await fetch(`http://${ip}:5000/user/password`, {
        method: "PUT",
        headers: {
          'Content-Type': 'application/json',
          'token': token,
        },
        body: JSON.stringify(body)
      });
  
      if (!response.ok) {
        const errorData = await response.json();
        throw new Error(errorData);
      }

      Toast.show({
        type: 'success',
        text1: 'Successfully Change Password!',
        visibilityTime: 3000, // 3 seconds
      });
   
      setOldPassword('');
      setNewPassword('');
      setConfirmPassword('');

      setEditModalVisible(false);
    } catch (error) {
      console.error(error.message);
      Alert.alert('Password Edit Error', error.message);
    }
  };

  //profile model clear
  const handleCloseEditModal = () => {
    // Clear input fields by resetting state variables
    setName(profile.user_name);
    setOldPassword('');
    setNewPassword('');
    setConfirmPassword('');
    // Toggle off the edit modal visibility
    setEditModalVisible(false);
  };

  //add vehicle
  const addCar = async() => {
    try{
      const token = await AsyncStorage.getItem('token');
      
      let requestBody = {
        vehicleNo: vehicleNumber,
        vehicleType: selectedVehicleType,
      };

      const response = await fetch(`http://${ip}:5000/user/addVehicle`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'token':token
        },
        body: JSON.stringify(requestBody), // Pass requestBody directly
      });
     
      if (!response.ok) {
        const errorData = await response.json();
        throw new Error(errorData); // Throw backend error message
      }
      setVehicle();
      Toast.show({
        type: 'success',
        text1: 'Successfully added vehicle',
        visibilityTime: 3000,
      });
      getVehicle()
      setModalVisible(false);

    } catch (error) {
      console.error(error.message);
      Alert.alert('Add Vehicle Error', error.message);
    }
  };
  
    //update username
    const handleDeleteVehicle = async v_id => {
      try {
        const token = await AsyncStorage.getItem('token');
    
        const response = await fetch(`http://${ip}:5000/user/username/${v_id}`, {
          method: "DELETE",
          headers: {
            'Content-Type': 'application/json',
            'token': token,
          },
          body: JSON.stringify(body)
        });
    
        if (!response.ok) {
          const errorData = await response.json();
          throw new Error(errorData);
        }
    
        getVehicle();
        Toast.show({
          type: 'success',
          text1: 'Successfully Delete Your Vehicle!',
          visibilityTime: 3000, // 3 seconds
        });
  
      } catch (error) {
        console.error(error.message);
        Alert.alert('Delete Vehicle Error', error.message);
      } 
    };

  //logout
  const logout = async () => {
    try {
      await AsyncStorage.removeItem('token');
      setIsAuthenticated(false);
      setLogoutConfirmationVisible(false);
      Toast.show({
        type: 'success',
        text1: 'Successfully logged out!',
        visibilityTime: 3000, // 3 seconds
      });
    } catch (error) {
      console.error('Error removing token from AsyncStorage:', error);
    }
  };
    
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="light-content" backgroundColor="#1A1B18" />
      <View style={styles.topp}>
        <Text style={styles.title}>My Profile</Text>
        <TouchableOpacity style={styles.profileArrow} onPress={()=>setLogoutConfirmationVisible(true)}>
            <Icon name="sign-out" size={20} color="white" />
          </TouchableOpacity>
      </View>
      <ScrollView>
        {/* profile */}
        <View style={styles.overall}>
          <Text style={styles.label1}>My Account Info</Text>
          <View style={styles.editContainer}>
            <TouchableOpacity onPress={() => setEditModalVisible(true)}>
              <Text style={styles.edit}>Edit Profile</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.name}>
          <Text style={styles.nametext}>{profile ? profile.user_name : 'loading'}</Text>
        </View>
        <View style={styles.name}>
          <Text style={styles.nametext}>{profile.phoneno===null ? profile.email : profile.phoneno}</Text>
        </View>

        {/* vehicle */}
        <View style={styles.overallvehicle}>
          <Text style={styles.label1}>My Vehicle Info</Text>
          <View style={styles.editContainer}>
            <TouchableOpacity onPress={() => setModalVisible(true)}>
              <Text style={styles.edit}>Add Vehicle</Text>
            </TouchableOpacity>
          </View>
        </View>
        {vehicle ? (
            vehicle.map((item, index) => (
              <View key={index}>
                <View style={styles.horizon}>
                  <View style={styles.horizontalLineFirstHalf} />
                  <Text style={styles.vehicle}>Vehicle {index + 1}</Text>
                  <View style={styles.horizontalLineSecondHalf} />
                </View>
                <View style={styles.name}>
                  <Text style={styles.nametext}>{item.vehicleno}</Text>
                </View>
                <View style={styles.name1}>
                  <Text style={styles.nametext}>{item.type}</Text>
                </View>
                <TouchableOpacity style={styles.delete} onPress={()=>setDeleteConfirmationVisible(true)}>
                  <Icon name="trash" size={20} color="#929291" />
                </TouchableOpacity>
              </View>
            ))
          ) : (
            <View style={styles.name}>
              <Text style={styles.nametext}>Loading...</Text>
            </View>
          )}


      </ScrollView>

      {/* add vehicle */}
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(false);
        }}
      >
        <View style={styles.modalBackground}>
          <View style={styles.modalView}>
            <TouchableOpacity style={styles.closeButton} onPress={handleCloseModal}>
              <Icon name="times" size={20} color="rgba(255, 255, 255, 0.5)" />
            </TouchableOpacity>
            <Text style={styles.modalTitle}>Add new vehicle</Text>
            <TextInput
              style={styles.input}
              placeholder="Vehicle Number"
              placeholderTextColor="rgba(255, 255, 255, 0.5)"
              onChangeText={setVehicleNumber}
              value={vehicleNumber}
            />
            <View style={styles.pick}>
              <Picker
                selectedValue={selectedVehicleType}
                style={styles.picker}
                onValueChange={(itemValue, itemIndex) =>
                  setSelectedVehicleType(itemValue)
                }
                dropdownIconColor="rgba(255, 255, 255, 0.5)"
              >
                <Picker.Item label="Light Vehicle" value="light" />
                <Picker.Item label="Two Wheelers" value="twoWheeler" />
              </Picker>
            </View>

            <TouchableOpacity
              style={styles.addButton}
              onPress={addCar}
            >
              <Text style={styles.addButtonText}>Add</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>

      {/* profile model */}
      <Modal
        animationType="slide"
        transparent={true}
        visible={editModalVisible}
        onRequestClose={() => {
          setEditModalVisible(false);
        }}
      >
        <View style={styles.modalBackground}>
          <View style={styles.modalView}>
            <TouchableOpacity style={styles.closeButton} onPress={handleCloseEditModal}>
              <Icon name="times" size={20} color="rgba(255, 255, 255, 0.5)" />
            </TouchableOpacity>
            <Text style={styles.modalTitle}>My Account Info</Text>
            <ScrollView>
            <TextInput
              style={styles.input}
              placeholder="Full Name"
              placeholderTextColor="rgba(255, 255, 255, 0.5)"
              onChangeText={setName}
              value={name}
            />
            <TouchableOpacity
              style={styles.addButton}
              onPress={handleEditUserName}
            >
              <Text style={styles.addButtonText}>Update</Text>
            </TouchableOpacity>
            <TextInput
              style={styles.input}
              placeholder="Old Password"
              placeholderTextColor="rgba(255, 255, 255, 0.5)"
              secureTextEntry={true}
              onChangeText={setOldPassword}
              value={oldPassword}
            />
            <TextInput
              style={styles.input}
              placeholder="New Password"
              placeholderTextColor="rgba(255, 255, 255, 0.5)"
              secureTextEntry={true}
              onChangeText={setNewPassword}
              value={newPassword}
            />
            <TextInput
              style={styles.input}
              placeholder="Confirm Password"
              placeholderTextColor="rgba(255, 255, 255, 0.5)"
              secureTextEntry={true}
              onChangeText={setConfirmPassword}
              value={confirmPassword}
            />
            <TouchableOpacity
              style={styles.addButton}
              onPress={handleEditPassword}
            >
              <Text style={styles.addButtonText}>Update</Text>
            </TouchableOpacity>
            </ScrollView>
          </View>
        </View>
      </Modal>

      {/* logout */}
      <Modal
        animationType="slide"
        transparent={true}
        visible={logoutConfirmationVisible}
        onRequestClose={() => setLogoutConfirmationVisible(false)}
      >
        <View style={styles.modalBackground}>
          <View style={styles.modalView}>
            <Text style={styles.modalTitle}>Log out of your account?</Text>
            <View style={styles.buttonContainer}>
              <TouchableOpacity
                style={styles.modalButton}
                onPress={() => setLogoutConfirmationVisible(false)} // If yes is pressed
              >
                <Text style={styles.modalButtonText}>CANCEL</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.modalButton}
                onPress={logout} // If no is pressed
              >
                <Text style={[styles.modalButtonText, {color:'red'}]}>LOG OUT</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
      {/* delete vehicle */}
      <Modal
  animationType="slide"
  transparent={true}
  visible={deleteConfirmationVisible}
  onRequestClose={() => setDeleteConfirmationVisible(false)}
>
  <View style={styles.modalBackground}>
    <View style={styles.modalView}>
      <Text style={styles.modalTitle}>Are you sure you want to unregister the vehicle?</Text>
      <View style={styles.buttonContainer}>
        <TouchableOpacity
          style={styles.modalButton}
          onPress={()=>setDeleteConfirmationVisible(false)} // If cancel is pressed
        >
          <Text style={styles.modalButtonText}>CANCEL</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.modalButton}
           // If delete is pressed
        >
          <Text style={[styles.modalButtonText, { color: 'red' }]}>DELETE</Text>
        </TouchableOpacity>
      </View>
    </View>
  </View>
</Modal>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1A1B18', 
  },
  overall: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 25, 
    marginTop: '7%', 
  },
  overallvehicle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 25, 
    marginTop: '10%', 
  },
  topp: {
    flexDirection: 'row',
    marginTop: '3%'
  },
  profileArrow: {
    position: 'absolute',
    top: '15%',
    right:'7%',
    zIndex: 1,
  },
  title: {
    fontSize: 21,
    fontWeight: '500',
    color: 'white',
    flexDirection: 'row',
    left: '7%',
  },
  label1: {
    fontSize: 15,
    fontWeight: '500',
    color: '#7C7C7C',
    textAlign: 'left',
  },
  editContainer: {
    flex: 1,
    alignItems: 'flex-end', 
  },
  edit: {
    fontSize: 15,
    fontWeight: '500',
    color: '#C5E274',
    textAlign: 'right',
  },
  name:{
    backgroundColor:'#242422',
    marginHorizontal: "7%",
    marginTop: 15,
  },
  button: {
    backgroundColor: '#CBE877',
    paddingVertical: 15,
    borderRadius: 40,
    marginBottom: 20,
    marginHorizontal: "7%",
    marginTop: 15,
  },
  buttonText: {
    color: '#1A1B18',
    fontSize: 16,
    textAlign: 'center',
    fontWeight: '600',
  },
  name1:{
    backgroundColor:'#242422',
    marginHorizontal: "7%",
    marginTop: 15,
    marginBottom:'3%'
  },
  nametext:{
    color:'#929291',
    padding:'5%',
    marginLeft:'5%',
    fontSize:15
  },
  horizontalLineFirstHalf: {
    borderBottomColor: '#7C7C7C',
    borderBottomWidth: 1,
    flex: 0.5, 
    marginVertical: 20,
    marginLeft:"13%" 
  },
  horizontalLineSecondHalf: {
    borderBottomColor: '#7C7C7C',
    borderBottomWidth: 1,
    flex: 0.5, 
    marginVertical: 20,
    marginRight:"13%"
  },
  horizon: {
    flexDirection: 'row',
    alignItems:'center'
  },
  horizon1: {
    flexDirection: 'row',
    alignItems:'center',
  },
  delete:{
    position: 'absolute',
    top: '15%',
    right:'7%',
    zIndex: 1,
  },
  vehicle: {
    color: '#7C7C7C',
    paddingHorizontal: 10,
  },
  modalBackground: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    justifyContent: 'center',
  },
  modalView: {
    backgroundColor: '#242422',
    borderRadius: 20,
    padding: 35,
    marginHorizontal:'5%',
    elevation: 5,
  },
  modalTitle: {
    fontSize: 17,
    marginBottom: 25,
    fontWeight:'400',
    color: 'rgba(255, 255, 255, 0.7)',
  },
  input: {
    width: '100%',
    padding: 10,
    marginBottom: 20,
    borderColor:'rgba(255, 255, 255, 0.1)',
    borderWidth:1,
    borderRadius: 10, 
    color:'white',
    fontSize:15,    
  },
  picker: {
    color: 'rgba(255, 255, 255, 0.5)',
    fontSize: 15,
    width: '100%',
  },
  pick:{   
    marginBottom: 20,
    borderColor: 'rgba(255, 255, 255, 0.1)', 
    borderWidth: 1, 
    borderRadius: 10, 
  },
  addButton: {
    backgroundColor: '#C5E274',
    padding: 13,
    borderRadius: 25,
    alignItems: 'center',
    width: '100%',
    marginHorizontal:0,
    paddingHorizontal:0,
  },
  addButtonText: { 
    color: '#0A1C1F',
    fontSize: 16,
    fontWeight: '600',
  },  
  closeButton: {
    position: 'absolute',
    padding:'15%',
    left:'90%',
    zIndex: 1,
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  modalButton: {
    width: '30%',
    alignItems: 'right',
  },
  modalButtonText: {
    color: 'white',
    fontSize: 15,
    fontWeight: '500',
    textAlign:'right'
  },
});
export default ProfileScreen;
