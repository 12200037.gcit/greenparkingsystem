import React, { useState } from 'react';
import { Text, Alert, View, StyleSheet, TouchableOpacity, SafeAreaView, Modal, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { useNavigation } from '@react-navigation/native';
import { ip } from '../../utils/config';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Toast from 'react-native-toast-message';
import * as Device from 'expo-device';
import * as Notifications from 'expo-notifications';
import { Platform } from 'react-native';
import Constants from 'expo-constants';

const MyParkingStatusScreen = ({route}) => {
  const { data, setTodo } = route.params;

  const navigation = useNavigation();
  const [modalVisible, setModalVisible] = useState(false);
  const [feedbackModalVisible, setFeedbackModalVisible] = useState(false);
  const [selectedOption, setSelectedOption] = useState(null);

  const handleParking = () => {
    navigation.goBack();
  };

  async function subscribeToNotifications() {
    let token;
    if (Platform.OS === "android") {
      await Notifications.setNotificationChannelAsync("default", {
        name: "default",
        importance: Notifications.AndroidImportance.MAX,
        vibrationPattern: [0, 250, 250, 250],
        lightColor: "#FF231F7C",
      });
    }
    if (Device.isDevice) {
      const { status: existingStatus } =
        await Notifications.getPermissionsAsync();
      if (existingStatus !== "granted") {
        const { status } = await Notifications.requestPermissionsAsync();
        if (status !== "granted") {
          alert("Failed to get permissions");
          return;
        }
      }
      token = (
        await Notifications.getExpoPushTokenAsync({
          projectId: Constants.expoConfig?.extra?.projectId,
        })
      ).data;
      console.log("Token EXPO", token);
    } else {
      alert("Must use physical device for Push Notifications");
    }

    return token;
  }
  // Function to send push notification to collector
  const handleNotifyCollector= async () => {
    try {
      const token = await AsyncStorage.getItem('token');
      const expoPushToken = await subscribeToNotifications();

      let requestBody = {
        area: data.area,
        user_name: data.user_name,
        user_id: data.user_id,
        a_id: data.a_id,
        starttime: data.starttime,
        vehicleno: data.vehicleno,
        expoPushToken: expoPushToken, // Include Expo push token in the request body
      };

      const response = await fetch(`http://${ip}:5000/user/notify/`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'token': token
        },
        body: JSON.stringify(requestBody),
      });

      if (!response.ok) {
        const errorData = await response.json();
        throw new Error(errorData); // Throw backend error message
      }

      setTodo(true);
      // setModalVisible(true);
      navigation.navigate('Home');
      Toast.show({
        type: 'success',
        text1: 'Successfully Notified Collector',
        visibilityTime: 3000,
      });

    } catch (error) {
      console.error(error.message);
      Alert.alert('Notification Error', error.message);
    }
  };

  // //notify collector
  // const handleNotifyCollector = async() => {
  //   try{
  //     const token = await AsyncStorage.getItem('token');
      
  //     let requestBody = {
  //       area: data.area,
  //       user_name: data.user_name,
  //       user_id: data.user_id,
  //       a_id: data.a_id,
  //       starttime: data.starttime,
  //       vehicleno: data.vehicleno,
  //     };

  //     const response = await fetch(`http://${ip}:5000/user/notify/`, {
  //       method: 'POST',
  //       headers: {
  //         'Content-Type': 'application/json',
  //         'token':token
  //       },
  //       body: JSON.stringify(requestBody), // Pass requestBody directly
  //     });
     
  
  //     if (!response.ok) {
  //       const errorData = await response.json();
  //       throw new Error(errorData); // Throw backend error message
  //     }
  //     setTodo(true)
  //     // setModalVisible(true);
  //     navigation.navigate('Home')
  //     Toast.show({
  //       type: 'success',
  //       text1: 'Successfully Notified Collector',
  //       visibilityTime: 3000,
  //     });

  //   } catch (error) {
  //     console.error(error.message);
  //     Alert.alert('Notification Error', error.message);
  //   }
  // };

  const handleLeaveFeedback = () => {
    setFeedbackModalVisible(true);
  };

  const closeModal = () => {
    setModalVisible(false);
  };

  const closeFeedbackModal = () => {
    setFeedbackModalVisible(false);
  };

  //time convertion
  const convertTo12HourTime = (timeString) => {
      if (!timeString) {
        return 'N/A'; // Or any default value to indicate that the time is not available
      }
    
      const [hours, minutes] = timeString.split(':').map(Number);
      const period = hours >= 12 ? 'PM' : 'AM';
      const displayHours = hours % 12 || 12;
      return `${displayHours}:${minutes < 10 ? '0' : ''}${minutes} ${period}`;
  };
    
  //date convertion
  const formatDate = (dateString) => {
      const date = new Date(dateString);
      const options = { day: 'numeric', month: 'long', year: 'numeric' };
      return date.toLocaleDateString('en-GB', options);
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.topmost}>
        <TouchableOpacity style={styles.arrow} onPress={handleParking}>
          <Icon name="arrow-left" size={20} color="#7C7C7C" />
        </TouchableOpacity>
        <Text style={styles.title}>My Parking Status</Text>
      </View>
      <View style={styles.description}>
        <Text style={styles.descr}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
        </Text>
      </View>
      <View style={styles.modalBackground}>
        <View style={styles.modalView}>
          <View style={styles.detailsContainer}>

            {/* Parking details */}
            <View style={styles.detailItem}>
              <Text style={styles.detailLabel}>Area</Text>
              <Text style={styles.detailValue}>:  {data.area}</Text>
            </View>
            <View style={styles.detailItem}>
              <Text style={styles.detailLabel}>Start Time</Text>
              <Text style={styles.detailValue}>:  {convertTo12HourTime(data.starttime)}</Text>
            </View>
            <View style={styles.detailItem}>
              <Text style={styles.detailLabel}>Total Time</Text>
              <Text style={styles.detailValue}>:  2 hours(not yet)</Text>
            </View>
            <View style={styles.detailItem}>
              <Text style={styles.detailLabel}>Total Amount</Text>
              <Text style={styles.detailValue}>:  Nu. 50(not yet)</Text>
            </View>
          </View>
        </View>
        <TouchableOpacity style={styles.parkButton} onPress={handleNotifyCollector}>
          <Text style={styles.parkButtonText}>Notify Collector</Text>
        </TouchableOpacity>
      </View>

      {/* Modal for Notify Collector */}
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={closeModal}
      >
        <View style={styles.modalContainer}>
          <View style={styles.modalContent}>
            <TouchableOpacity style={styles.skipButton} onPress={closeModal}>
              <Text style={styles.skipButtonText}>Skip</Text>
            </TouchableOpacity>
            <Text style={styles.modalText}>Wait for collector action!</Text>
            <TouchableOpacity style={styles.feedbackButton} onPress={handleLeaveFeedback}>
              <Text style={styles.feedbackButtonText}>Leave your feedback</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>

      {/* Modal for Leave Feedback */}
      <Modal
        animationType="slide"
        transparent={true}
        visible={feedbackModalVisible}
        onRequestClose={closeFeedbackModal}
      >
        <View style={styles.modalContainer}>
          <View style={styles.modalContent}>
            <TouchableOpacity style={styles.skipButton} onPress={closeFeedbackModal}>
              <Text style={styles.skipButtonText}>Skip</Text>
            </TouchableOpacity>
            <Text style={styles.modalText}>Give feedback to the collector</Text>
            {/* Radio buttons for feedback options */}
            <View style={styles.radioContainer}>
              <TouchableOpacity
                style={styles.radioButton}
                onPress={() => setSelectedOption('Polite')}
              >
                <View style={[styles.radio, { borderColor: selectedOption === 'Polite' ? 'rgba(255, 255, 255, 0.5)' : 'rgba(255, 255, 255, 0.5)' }]}>
                  {selectedOption === 'Polite' && <View style={styles.selectedRadio} />}
                </View>
                <Text style={styles.radioButtonText}>Polite</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.radioButton}
                onPress={() => setSelectedOption('Verbal Harassment')}
              >
                <View style={[styles.radio, { borderColor: selectedOption === 'Verbal Harassment' ? 'rgba(255, 255, 255, 0.5)' : 'rgba(255, 255, 255, 0.5)' }]}>
                  {selectedOption === 'Verbal Harassment' && <View style={styles.selectedRadio} />}
                </View>
                <Text style={styles.radioButtonText}>Verbal Harassment</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.radioButton}
                onPress={() => setSelectedOption('Vulgar Language')}
              >
                <View style={[styles.radio, { borderColor: selectedOption === 'Vulgar Language' ? 'rgba(255, 255, 255, 0.5)' : 'rgba(255, 255, 255, 0.5)' }]}>
                  {selectedOption === 'Vulgar Language' && <View style={styles.selectedRadio} />}
                </View>
                <Text style={styles.radioButtonText}>Vulgar Language</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.radioButton}
                onPress={() => setSelectedOption('Punctual')}
              >
                <View style={[styles.radio, { borderColor: selectedOption === 'Punctual' ? 'rgba(255, 255, 255, 0.5)' : 'rgba(255, 255, 255, 0.5)' }]}>
                  {selectedOption === 'Punctual' && <View style={styles.selectedRadio} />}
                </View>
                <Text style={styles.radioButtonText}>Punctual</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.radioButton}
                onPress={() => setSelectedOption('Rude Behavior')}
              >
                <View style={[styles.radio, { borderColor: selectedOption === 'Rude Behavior' ? 'rgba(255, 255, 255, 0.5)' : 'rgba(255, 255, 255, 0.5)' }]}>
                  {selectedOption === 'Rude Behavior' && <View style={styles.selectedRadio} />}
                </View>
                <Text style={styles.radioButtonText}>Rude Behavior</Text>
              </TouchableOpacity>

                <Text style={styles.otherText}>Other</Text>
                <View style={styles.textAreaContainer}>
                  <TextInput
                    style={styles.textArea} 
                    placeholder="Write your feedback..."
                    placeholderTextColor='rgba(255, 255, 255, 0.2)'
                    multiline={true}
                    // textAlignVertical="top"
                    // onChangeText={(text) => console.log(text)}
                  />
                    </View>
            </View>
            <TouchableOpacity style={styles.leaveFeedbackButton}>
              <Text style={styles.leaveFeedbackButtonText}>Leave your feedback</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1A1B18',
  },
  topmost: {
    flexDirection: 'row',
    marginTop: '3%'
  },
  arrow: {
    position: 'absolute',
    top: '15%',
    left: '7%',
    zIndex: 1,
  },
  title: {
    fontSize: 21,
    fontWeight: '500',
    color: 'white',
    flexDirection: 'row',
    marginLeft: '15%',
  },
  description: {
    alignItems: 'center',
    marginBottom: '10%',
    marginHorizontal: '5%',
    marginTop: '10%',
  },
  descr: {
    color: 'rgba(255, 255, 255, 0.5)',
    fontSize: 14,
    fontWeight: '400',
  },
  modalBackground: {
    flex: 1,
    backgroundColor: '#242422',
    marginHorizontal: '7%',
    marginTop: 0,
    borderRadius: 20,
    marginVertical: '45%',
  },
  modalView: {
    padding: 35,
    width: '100%',
    alignItems: 'left',
  },
  detailsContainer: {
    marginTop: 10,
  },
  detailItem: {
    flexDirection: 'row',
    marginBottom: 15,
  },
  detailLabel: {
    flex: 1,
    color: '#929291',
    fontSize: 15,
    fontWeight: '400',
  },
  detailValue: {
    color: '#929291',
    flex: 1.5,
    fontWeight: '400',
    fontSize: 15,
    textAlign: 'left',
    textTransform: 'capitalize'
  },
  parkButton: {
    backgroundColor: '#CBE877',
    marginHorizontal: '7%',
    paddingVertical: 13,
    borderRadius: 30,
    marginBottom: 10,
    marginTop: -8,
  },
  parkButtonText: {
    color: '#0A1C1F',
    fontSize: 17,
    fontWeight: '600',
    textAlign: 'center',
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  skipButton: {
    position: 'absolute',
    top: 10,
    right: 10,
    padding: 10,
    textAlign:'right'
  },
  skipButtonText: {
    color: 'rgba(255, 255, 255, 0.5)',
    fontSize: 13,
    fontWeight:'400'
  },
  modalContent: {
    backgroundColor: '#242422',
    padding: 50,
    borderRadius: 20,
    alignItems: 'flex-start',
  },
  modalText: {
    fontSize: 15,
    fontWeight: '400',
    // marginBottom: 0,
    color: 'rgba(255, 255, 255, 1)',
    marginTop: 5,
  },
  feedbackButton: {
    backgroundColor: '#CBE877',
    paddingVertical: 13,
    paddingHorizontal: 35,
    borderRadius: 30,
    marginTop:30
  },
  feedbackButtonText: {
    fontSize: 17,
    fontWeight: '600',
    color: '#0A1C1F',
  },
  radioContainer: {
    marginTop: 20,
    alignItems: 'flex-start', // Align to left
    // marginLeft: 35, // Add left margin for better alignment
  },
  radioButton: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginBottom: 20,
  },
  radio: {
    height: 20,
    width: 20,
    borderRadius: 10,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  selectedRadio: {
    height: 10,
    width: 10,
    borderRadius: 5,
    backgroundColor: 'rgba(255, 255, 255, 0.5)',
  },
  radioButtonText: {
    fontSize: 13,
    color: 'rgba(255, 255, 255, 0.5)',
    marginLeft: 10,
  },
  textAreaContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    flexWrap: 'wrap',
    width: '92%', 
  },
  otherText: {
    fontSize: 13,
    color: 'rgba(255, 255, 255, 0.5)',
    marginRight: 10, 
  },
  textArea: {
    flex: 1, 
    textAlignVertical:'top',
    backgroundColor: '#242422',
    height: 100,
    marginTop: 10,
    borderRadius: 10,
    borderColor: 'rgba(255, 255, 255, 0.25)',
    borderWidth: 1,
    paddingHorizontal: 20,
    paddingVertical:20,
    color: 'white',
  },  
  leaveFeedbackButton: {
    backgroundColor: '#CBE877',
    paddingVertical: 13,
    paddingHorizontal: 40,
    borderRadius: 30,
    marginTop: 20,
  },
  leaveFeedbackButtonText: {
    fontSize: 17,
    fontWeight: '600',
    color: '#0A1C1F',
  },
});

export default MyParkingStatusScreen;
