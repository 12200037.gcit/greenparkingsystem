import React, {useState, useEffect} from 'react';
import { View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import { useNavigation } from '@react-navigation/native';

const MyCurrentParkingScreen = ({setTodo, data}) => {
    const navigation = useNavigation();

    // console.log(data.starttime)

    const [timer, setTimer] = useState("00:00");

    // function calculateTimer(starttime) {
    //     // Split the starttime string into its components
    //     const [timePart, millisecondsPart] = starttime.split('.');
    //     const [hours, minutes, seconds] = timePart.split(':').map(Number);
    //     const milliseconds = millisecondsPart ? parseInt(millisecondsPart) : 0;
    
    //     // Create a new Date object with the given time components
    //     const startTimeDate = new Date();
    //     startTimeDate.setHours(hours, minutes, seconds, milliseconds);
    
    //     // Get the current time
    //     const currentTime = new Date();
    
    //     // Calculate the difference in milliseconds
    //     const difference = currentTime.getTime() - startTimeDate.getTime();
    
    //     // Calculate hours, minutes, and seconds from the difference
    //     const diffHours = Math.floor(difference / (1000 * 60 * 60));
    //     const diffMinutes = Math.floor((difference % (1000 * 60 * 60)) / (1000 * 60));
    //     const diffSeconds = Math.floor((difference % (1000 * 60)) / 1000);
    
    //     // Format the timer
    //     return `${String(diffHours).padStart(2, '0')}:${String(diffMinutes).padStart(2, '0')}:${String(diffSeconds).padStart(2, '0')}`;
    // }
    
    // useEffect(() => {
    //     // Calculate timer based on starttime
    //     setTimer(calculateTimer(data.starttime));
    
    //     // Update timer every second
    //     const interval = setInterval(() => {
    //         setTimer(calculateTimer(data.starttime));
    //     }, 1000);
    
    //     // Clean up interval on component unmount
    //     return () => clearInterval(interval);
    // }, [data.starttime]);
    
    
    const handleParkingStatus = () => {
        navigation.navigate('MyParkingStatus',{data, setTodo});
      };

  return (
       <TouchableOpacity style={styles.all} onPress={handleParkingStatus}>
        <Text style={styles.current}>My current parking status</Text>
        <View style={styles.secondView}>
            <Text style={styles.lot}>{data.area}</Text>
        <Text style={styles.desc}>Area Name</Text>
        </View>
        <View style={styles.secondView}>
            <Text style={styles.lot}>{timer}<Text style={styles.nu}>min</Text> </Text>
            <Text style={styles.desc}>Time</Text>
        </View>
        {/* <View style={styles.secondView}>
            <Text style={styles.lot}><Text style={styles.nu}>Nu.</Text>25</Text>
            <Text style={styles.desc}>Amount</Text>
        </View> */}
        </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
    all: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      alignItems: 'baseline',
      marginBottom: '3%', 
      marginHorizontal: '10%',
      position: 'absolute',
      backgroundColor:'#242422',
      padding:20,
      bottom: 0, 
      borderRadius:25,
      width: '100%', 
      paddingVertical:30,
    },
    current: {
      color: 'rgba(255, 255, 255, 0.5)',
      fontSize: 12,
      fontWeight: '400',
      position: 'absolute',
      top: '40%',
      left: '10%',
      marginBottom: 20, // Adjust as needed
    },
    secondView: {
      flex: 1,
      alignItems: 'center',
      marginTop:'5%',
      justifyContent: 'flex-end', // Align text to the bottom within each secondView
    },
    lot: {
      color: '#CBE877',
      fontSize: 20,
      fontWeight: '400',
      textTransform: 'capitalize'
    },
    desc: {
      color: 'rgba(255, 255, 255, 0.5)',
      fontWeight: '400',
      fontSize: 10,
    },
    nu: {
      color: 'rgba(255, 255, 255, 0.5)',
      fontSize: 10,
    },
  });
  

export default MyCurrentParkingScreen;
