import React, { useState, useEffect } from 'react';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import LoginPage from '../screens/auth/Login';
import SignupPage from '../screens/auth/SignUp';
import UserTab from './UserTab';
import MyCurrentParkingScreen from '../screens/stack/MyCurrentParkingStatus';
import MyParkingStatusScreen from '../screens/stack/MyParkingStatus';
import AsyncStorage from '@react-native-async-storage/async-storage';
import ForgotPasswordPage from '../screens/auth/ForgotPassword'
import { ip } from '../utils/config';

import { LogBox } from 'react-native';

LogBox.ignoreLogs([
  'Non-serializable values were found in the navigation state'
]);

const Stack = createStackNavigator();

const UserStack = () => {

  const [isAuthenticated, setIsAuthenticated] = useState(false);

  const checkAuthenticated = async () => {
    try {
      const token = await AsyncStorage.getItem('token');
      const response = await fetch(`http://${ip}:5000/auth/verify`, {
        method: "GET",
        headers: { 
          'Content-Type': 'application/json',
          'token': token 
        }
      });
  
      const parseRes = await response.json();
  
      parseRes === true ? setIsAuthenticated(true) : setIsAuthenticated(false);
    } catch (error) {
      console.error(error.message);
    }
  };

  useEffect(() => {
    checkAuthenticated();
  }, []);
  
  return (
    <Stack.Navigator    
          screenOptions={{
            headerShown: false,
            gestureEnabled: true,
            ...TransitionPresets.SlideFromRightIOS, // Use SlideFromRightIOS transition
            }}>
            
          {!isAuthenticated ? (
                    <>
                      <Stack.Screen
                        name="Login"
                        component={LoginPage}
                        options={{ headerShown: false}}
                        initialParams={{ setIsAuthenticated }}
                      />
                      <Stack.Screen
                        name="SignUp"
                        component={SignupPage}
                        options={{ headerShown: false}}
                        initialParams={{ setIsAuthenticated }}
                      />
                       <Stack.Screen
                        name="ForgotPassword"
                        component={ForgotPasswordPage}
                        options={{ headerShown: false}}
                        // initialParams={{ setIsAuthenticated }}
                      />
                    </>
                  ) : (
                    <>
                      <Stack.Screen
                        name="HomeS"
                        component={UserTab}
                        initialParams={{ setIsAuthenticated }}
                      />
                      <Stack.Screen
                        name="MyCurrentParking"
                        component={MyCurrentParkingScreen}
                        initialParams={{ setIsAuthenticated }}
                      />
                      <Stack.Screen
                        name="MyParkingStatus"
                        component={MyParkingStatusScreen}
                        initialParams={{ setIsAuthenticated }}
                      />
                    </>
                  )}

        </Stack.Navigator>
  )
}

export default UserStack