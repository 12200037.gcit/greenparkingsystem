import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomeScreen from '../screens/tab/Home';
import HistoryScreen from '../screens/tab/HistoryScreen';
import ProfileScreen from '../screens/tab/ProfileScreen';
import { Ionicons } from '@expo/vector-icons';

const Tab = createBottomTabNavigator();

const UserTab = ({route}) => {
  const {setIsAuthenticated} = route.params;
  return (
    <Tab.Navigator 
      screenOptions={{
        headerShown: false,
        tabBarActiveTintColor: '#CBE877', // Set the color of the active tab icon and label
        tabBarStyle: {
          backgroundColor: '#242422',
          borderColor:'#242422',
          // borderTopRightRadius:12,
          // borderTopLeftRadius:12,
        },
      }}
    >
      <Tab.Screen
        name="Home"
        component={HomeScreen}
        initialParams={{ setIsAuthenticated }}
        options={{
          tabBarIcon: ({ color, size }) => (
            <Ionicons name="home-outline" size={size} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name="History"
        component={HistoryScreen}
        options={{
          tabBarIcon: ({ color, size }) => (
            <Ionicons name="time-outline" size={size} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          tabBarIcon: ({ color, size }) => (
            <Ionicons name="person-outline" size={size} color={color} />
          ),
        }}
        initialParams={{setIsAuthenticated}}
      />
    </Tab.Navigator>
  );
};

export default UserTab;
