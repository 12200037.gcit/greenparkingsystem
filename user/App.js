import React from 'react';
import { StatusBar } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import UserStack from './src/navigation/UserStack';
import Toast from 'react-native-toast-message';

export default function App() {
  return (
    <NavigationContainer>
      <UserStack/>
      <Toast/>
    </NavigationContainer>  
  );
}

