import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import CollectorStack from './src/navigation/CollectorStack';
import Toast from 'react-native-toast-message';

export default function App() {

  return (
    <NavigationContainer>
      <CollectorStack/>
      <Toast/>
    </NavigationContainer>
    
  );
}

