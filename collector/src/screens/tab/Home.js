import React, {useEffect, useState} from 'react';
import { View, Text, Alert, StyleSheet, Image, ScrollView, TouchableOpacity, StatusBar } from 'react-native';
import { useNavigation } from '@react-navigation/native';  
import { AntDesign } from '@expo/vector-icons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Toast from 'react-native-toast-message';
import {ip} from '../../utils/config'

const HomeScreen = ({route}) => {
  const { setIsAuthenticated } = route.params;
  const [area, setArea] = useState([]);
  const [status, setStatus] = useState([]);
  const navigation = useNavigation();
  const [todo, setTodo] = useState(false);

  const handleViewPress = (marker) => {
    navigation.navigate('CurrentParkingDetails',{marker,area, setTodo});
  };

  //get areas
  const getArea = async () => {
    try {
      const token = await AsyncStorage.getItem('token');
      console.log(token)
      const response = await fetch(`http://${ip}:5000/collector/area/`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'token': token,
        },
      });

      if (!response.ok) {
        // If response status is not okay, extract and handle the error message
        const errorData = await response.json();
        throw new Error(errorData); // Throw backend error message
      }

      const data = await response.json();
      setArea(data[0]);

    } catch (error) {
      Alert.alert('Get Area Error', error.message);
    }
  };

  //get parking status
  const getStatus = async () => {
    try {
      const token = await AsyncStorage.getItem('token');

      const response = await fetch(`http://${ip}:5000/collector/status/`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'token': token,
        }
      });

      if (!response.ok) {
        // If response status is not okay, extract and handle the error message
        const errorData = await response.json();
        throw new Error(errorData); // Throw backend error message
      }

      const data = await response.json();
      setStatus(data);

    } catch (error) {
      console.error(error.message);
      Alert.alert('Get Status Error', error.message);
    }
  };


   //logout
   const logout = async () => {
    try {
      await AsyncStorage.removeItem('token');
      setIsAuthenticated(false);
      // navigation.reset('Login');
      Toast.show({
        type: 'success',
        text1: 'Successfully logged out!',
        visibilityTime: 3000, // 3 seconds
      });
    } catch (error) {
      console.error('Error removing token from AsyncStorage:', error);
    }
  };

  //time convertion
  const convertTo12HourTime = (timeString) => {
    if (!timeString) {
      return 'N/A'; // Or any default value to indicate that the time is not available
    }
    const [hours, minutes] = timeString.split(':').map(Number);
    const period = hours >= 12 ? 'PM' : 'AM';
    const displayHours = hours % 12 || 12;
    return `${displayHours}:${minutes < 10 ? '0' : ''}${minutes} ${period}`;
  };

  //fetch data
  useEffect(() => {
    getArea();
    getStatus();
    setTodo(false);
  }, [todo]);
  
  
  return (
    <View style={styles.container}>
          <StatusBar barStyle = "dark-content" backgroundColor="#1A1B18" /> 
    <TouchableOpacity style={styles.logoutButton} onPress={logout}>
        <AntDesign name="logout" size={24} color="white" />
      </TouchableOpacity>
      <View style={styles.img}>
        <Image
          source={require('../../../assets/plant.png')} 
          style={styles.image}
          resizeMode="contain"/>
      </View>
        <Text style={styles.title}>My Area Status</Text>
        <View style={styles.data}>
          <View style={styles.item}>
            <Text style={styles.area}>{area.area}</Text>
            <Text style={styles.name}>Area Name</Text>
          </View>
         <View style={styles.item}>
            <Text style={styles.area}>{area.totalslot}</Text>
            <Text style={styles.name}>totalslot</Text>
         </View>
          <View style={styles.item}>
            <Text style={styles.area}>{area.availableslots}</Text>
            <Text style={styles.name}>Avaliable</Text>
          </View>
        </View> 

        <View style={styles.park}>
          <View >
            <Text style={styles.parktitle}>Cars Parked</Text>
          </View>
              <View style={styles.a}>
                <Text style={styles.text}>Start Time</Text>
                <Text style={styles.text1}>Vehicle No</Text>
                <Text style={styles.text2}>Duration</Text>
              </View>
              <ScrollView style={styles.scrollView}>

              {status.length === 0 ? (
                <View style={styles.a}>
                  <Text style={styles.text2}>No User parking</Text>
                </View>
              ) : (
                status.map((item, index) => (
                  <TouchableOpacity key={index} onPress={() => handleViewPress(item)}>
                    <View style={styles.a}>
                      <Text style={styles.text}>{convertTo12HourTime(item.starttime)}</Text>
                      <Text style={styles.text1}>{item.vehicleno}</Text>
                      <Text style={styles.text2}>Not Set</Text>
                    </View>
                  </TouchableOpacity>
                ))
              )}

                        
                      
                  </ScrollView>
        </View>       
    </View>
  );
}; 
const styles = StyleSheet.create({
  logoutButton: {
    position: 'absolute',
    top: 20,
    right: 20,
  },
  a:{
    flexDirection:'row',
    justifyContent:'space-around',
    width:'100%',
    paddingVertical:15,
    borderRadius:10,
    backgroundColor:'#333',
    marginVertical:5,
    marginBottom: 10,
  },
  text:{
    color:'white'
  },
  text1:{
    color:'grey'
  },
  text2:{
    color:'#8ec545'
  },
  park:{
    marginTop:10,
    width:'100%'
  },
  parktitle:{
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 20,
    color: 'white',
  },
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: '#1A1B18', 
    paddingTop: 20, // Adjust as needed
    paddingLeft: 20,
    paddingRight: 20,
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 20,
    color: 'white',
  },
  image: {
    height:60,
    width:50,
    marginTop:-20,
    marginBottom:-10,
    paddingLeft: 10,
  },
  data:{
    // flex:1,
    height:50,
    flexDirection:'row',
    width:'100%',
    justifyContent:'space-around'
  },
  area:{
    color:'white',
    fontSize:20,
    fontWeight:'bold'
  },
  name:{
    color:'grey',
    fontSize:12,
  },
  item:{
    alignItems:'center'
  }
});

export default HomeScreen;