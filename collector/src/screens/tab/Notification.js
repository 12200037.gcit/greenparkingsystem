import React, {useEffect, useState} from 'react';
import { View, Text, Alert, StyleSheet, TouchableOpacity, Button, StatusBar } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { ScrollView } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import { AntDesign } from '@expo/vector-icons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Toast from 'react-native-toast-message';
import {ip} from '../../utils/config'
import * as Device from 'expo-device';
import * as Notifications from 'expo-notifications';
import { Platform } from 'react-native';
import Constants from 'expo-constants';

const NotificationScreen = ({route}) => {
  
  const { setIsAuthenticated } = route.params;
  const [todo, setTodo] = useState(false);
  const [noti, setNoti] = useState([])
  const navigation = useNavigation();
 
  const handleViewPress = (detail) => {
    navigation.navigate('ParkingDetails', {detail, setTodo});
  };

  //fetch notification
  const getNotification = async () => {
    try {
      const token = await AsyncStorage.getItem('token');
      
      const response = await fetch(`http://${ip}:5000/collector/notification/`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'token': token,
        },
      });

      if (!response.ok) {
        // If response status is not okay, extract and handle the error message
        const errorData = await response.json();
        throw new Error(errorData); // Throw backend error message
      }

      const data = await response.json();
      setNoti(data);

    } catch (error) {
      Alert.alert('Notification Error', error.message);  
    }
  };
  
  useEffect(()=>{
    getNotification();
    setTodo(false);
  },[todo])




    //time convertion
    const convertTo12HourTime = (timeString) => {
      if (!timeString) {
        return 'N/A'; // Or any default value to indicate that the time is not available
      }
    
      const [hours, minutes] = timeString.split(':').map(Number);
      const period = hours >= 12 ? 'PM' : 'AM';
      const displayHours = hours % 12 || 12;
      return `${displayHours}:${minutes < 10 ? '0' : ''}${minutes} ${period}`;
    };
    
  //logout
  const logout = async () => {
    try {
      await AsyncStorage.removeItem('token');
      setIsAuthenticated(false);
      // navigation.reset('Login');
      Toast.show({
        type: 'success',
        text1: 'Successfully logged out!',
        visibilityTime: 3000, // 3 seconds
      });
    } catch (error) {
      console.error('Error removing token from AsyncStorage:', error);
    }
  };

  return (
    <View style={styles.container}>
          <StatusBar barStyle = "light-content" backgroundColor="#1A1B18" /> 
      {/* <TouchableOpacity onPress={handleBackPress} style={styles.backButton}>
        <Ionicons name="arrow-back" size={24} color="white" /> */}
        <Text style={styles.title}>Notification</Text>
      {/* </TouchableOpacity> */}
      <TouchableOpacity style={styles.logoutButton} onPress={logout}>
        <AntDesign name="logout" size={24} color="white" />
      </TouchableOpacity>

      <View style={styles.detail}>
        <Text style={styles.t}>Waiting Users</Text>
      </View>
          <View style={styles.a}>
            <Text style={styles.text}>User</Text>
            <Text style={styles.text1}>Vehicle No</Text>
            <Text style={styles.text2}>End Time</Text>
          </View>

        <ScrollView style={styles.scrollView}>
          
        {noti.length === 0 ? (
          <View style={styles.a}>
            <Text style={styles.text2}>No Notifications</Text>
          </View>
        ) : (
          noti.map((item, index) => (
            <TouchableOpacity key={index} onPress={() => handleViewPress(item)}>
              <View style={styles.a}>
                <Text style={styles.text}>{item.user_name}</Text>
                <Text style={styles.text1}>{item.vehicleno}</Text>
                <Text style={styles.text2}>{convertTo12HourTime(item.endtime)}</Text>
              </View>
            </TouchableOpacity>
          ))
        )}

        </ScrollView>

    </View>
  );
};

const styles = StyleSheet.create({
  logoutButton: {
    position: 'absolute',
    top: 20,
    right: 20,
    zIndex: 1, // Ensure it's above other elements
  },
  container: {
    flex: 1,
    backgroundColor: '#1A1B18',
    paddingTop: 20,
    paddingLeft: 20,
    paddingRight: 20,
    color: 'white',
  },
  backButton: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    borderRadius: 5,
  },
  backButtonText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'white',
    marginLeft: 5,
  },
  title: {
    fontSize: 19,
    fontWeight: 'bold',
    color: 'white',
    paddingHorizontal: 10,
  },
  t: {
    fontSize: 14,
    color: 'white',
    paddingTop:20
  },
  a:{
    flexDirection:'row',
    justifyContent:'space-around',
    width:'100%',
    paddingVertical:15,
    borderRadius:10,
    backgroundColor:'#333',
    marginTop:15
  },
  text:{
    color:'white',
  },
  text1:{
    color:'grey'
  },
  text2:{
    color:'#8ec545'
  },
});
export default NotificationScreen;
