import React, { useState, useEffect } from 'react';
import { StyleSheet, Image, Text, View, TouchableOpacity, Button, Alert, Modal, TextInput,StatusBar } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { ScrollView } from 'react-native-gesture-handler';
import { Searchbar } from 'react-native-paper';
import { AntDesign } from '@expo/vector-icons';
import { ip } from '../../utils/config';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Toast from 'react-native-toast-message';

const HistoryScreen = ({route}) => {

    const { setIsAuthenticated } = route.params;
    const [searchQuery, setSearchQuery] = useState('');
    const [history, setHistory] = useState([]);
    const navigation = useNavigation();

      const handleViewPress = (marker) => {
        navigation.navigate('HistoryDetail', {marker});
      };
    
      //get history
      const getHistory = async () => {
        try {
          const token = await AsyncStorage.getItem('token');
      
          const response = await fetch(`http://${ip}:5000/collector/history/`, {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json',
              'token': token,
            },
          });
      
          if (!response.ok) {
            // If response status is not okay, extract and handle the error message
            const errorData = await response.json();
            throw new Error(errorData); // Throw backend error message
          }
      
          const data = await response.json();
      
          // Set the history state
          setHistory(data);
      
        } catch (error) {
          Alert.alert('History Error', error.message);
        }
      };
      useEffect(() => {
        getHistory();
      }, []); 
      
      //logout
  const logout = async () => {
    try {
      await AsyncStorage.removeItem('token');
      setIsAuthenticated(false);
      // navigation.reset('Login');
      Toast.show({
        type: 'success',
        text1: 'Successfully logged out!',
        visibilityTime: 3000, // 3 seconds
      });
      console.log('Token removed from AsyncStorage');
    } catch (error) {
      console.error('Error removing token from AsyncStorage:', error);
    }
  };

   //time convertion
   const convertTo12HourTime = (timeString) => {
    if (!timeString) {
      return 'N/A'; // Or any default value to indicate that the time is not available
    }
  
    const [hours, minutes] = timeString.split(':').map(Number);
    const period = hours >= 12 ? 'PM' : 'AM';
    const displayHours = hours % 12 || 12;
    return `${displayHours}:${minutes < 10 ? '0' : ''}${minutes} ${period}`;
  };
     
  return (
    <View style={styles.container}>

      <StatusBar barStyle = "light-content" backgroundColor="#1A1B18" /> 
      {/* <TouchableOpacity onPress={handleBackPress} style={styles.backButton}>
        <Ionicons name="arrow-back" size={24} color="white" /> */}
        <Text style={styles.title}>Today's History</Text>
      {/* </TouchableOpacity> */}

      <TouchableOpacity style={styles.logoutButton} onPress={logout}>
        <AntDesign name="logout" size={24} color="white" />
      </TouchableOpacity>

        {/* <View style={styles.detail}>
            <Text style={styles.t}>Contains the details
             about the car parking with following information </Text>
        </View> */}
            <Searchbar style= {styles.s}
                placeholder="Search Vehicle No."
                onChangeText={setSearchQuery}
                value={searchQuery}
                iconColor="white"
                placeholderTextColor="gray"
                fontSize={13} 
                textInputStyle={{ color: 'white' }} />

        <ScrollView style={styles.scrollView}>
          <View>
            
                <View style={styles.a}>
                    <Text style={styles.text}>History</Text>
                    <Text style={styles.text1}>S&E Time</Text>
                    <Text style={styles.text2}>Amount</Text>
                  </View>

              {history.map((item, index) => (
                <TouchableOpacity key={index} onPress={() => handleViewPress(item)}>
                  <View style={styles.a}>
                    <Text style={styles.text}>{item.h_id}</Text>
                    <Text style={styles.text1}>{convertTo12HourTime(item.starttime)} - {convertTo12HourTime(item.starttime)}</Text>
                    <Text style={styles.text2}>NU.{item.totalamount}/-</Text>
                  </View>
                </TouchableOpacity>
              ))}
    
          </View>
        </ScrollView>

    </View>
  );
};

const styles = StyleSheet.create({
  logoutButton: {
    position: 'absolute',
    top: 20,
    right: 20,
    zIndex: 1, // Ensure it's above other elements
  },
    container: {
        flex: 1,
        backgroundColor: '#1A1B18',
        paddingTop: 20,
        paddingLeft: 20,
        paddingRight: 20,
        color: 'white',
      },
      backButton: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 10,
        borderRadius: 5,
      },
      title: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'white',
        paddingHorizontal: 10,
      },
      t: {
        fontSize: 15,
        color: 'grey',
        flexBasis: 'auto',
        justifyContent: 'center',
        paddingVertical: 10,
      },
      a:{
        flexDirection:'row',
        justifyContent:'space-around',
        width:'100%',
        paddingVertical:15,
        borderRadius:10,
        backgroundColor:'#333',
        marginTop:15
      },
      text:{
        color:'white',
      },
      text1:{
        color:'grey'
      },
      text2:{
        color:'#8ec545'
      },
      s:{
        backgroundColor: 'transparent',
        borderWidth: 1, 
        borderColor: 'white',
        borderRadius: 20, 
        color: 'white',
      }
});

export default HistoryScreen;