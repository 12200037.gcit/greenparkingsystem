import React, { useState } from 'react';
import { View, TextInput, TouchableOpacity, Text, StatusBar, StyleSheet, Image, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome'
import AsyncStorage from '@react-native-async-storage/async-storage'; // Import AsyncStorage
import Toast from 'react-native-toast-message';
import { ip } from '../../utils/config';

const LoginPage = ({ navigation, route}) => {
  const { setIsAuthenticated } = route.params
  
  // State variables for email, phone number, password, and password visibility
  const [phoneNo, setPhoneNo] = useState('');
  const [password, setPassword] = useState('');
  const [isPasswordVisible, setPasswordVisible] = useState(false);

  const handleLogin = async () => {
    try {
      
      let requestBody = { phoneNo, password };

      const response = await fetch(`http://${ip}:5000/auth/loginCollector`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(requestBody),
      });
  
      if (!response.ok) {
        // If response status is not okay, extract and handle the error message
        const errorData = await response.json();
        throw new Error(errorData); // Throw backend error message
      }
  
      const data = await response.json();
          
      // Handle successful login
      await AsyncStorage.setItem('token', data.jwtToken);
      setIsAuthenticated(true)

      navigation.replace('HomeS', {setIsAuthenticated});
      // Alert.alert('Login Successful', 'Hey Welcome');
      Toast.show({
        type: 'success',
        text1: 'Successfully logged in',
        visibilityTime: 3000,
      });
    } catch (error) {
        // Alert.alert('Login Error', error.message)
        Toast.show({
          type: 'error',
          text1: 'Login Error',
          text2:error.message
        });
    }
  };
  
  // Function to toggle password visibility
  const togglePasswordVisibility = () => {
    setPasswordVisible(!isPasswordVisible);
  };

  return (
    <View style={styles.container}>
      <StatusBar barStyle="light-content" backgroundColor="#1A1B18" />
      <View style={styles.imageContainer}>
        <Image
          source={require('../../../assets/Login.jpeg')} 
          style={styles.image}
          resizeMode="contain"
        />
      </View>
      <View>
        <Text style={styles.login}>Login to your account</Text>
      </View>
      <View style={styles.inputContainer}>
        <Icon name="user" size={20} color="rgba(255, 255, 255, 0.5)" style={styles.inputIcon} />
        <TextInput
          style={styles.input}
          placeholder="Phone Number"
          placeholderTextColor="rgba(255, 255, 255, 0.5)"
          value={phoneNo}
          onChangeText={(text) => setPhoneNo(text)}
        />
      </View>
      <View style={styles.inputContainer}>
        <Icon name="lock" size={20} color="rgba(255, 255, 255, 0.5)" style={styles.inputIcon} />
        <TextInput
          style={styles.input}
          placeholder="Password"
          placeholderTextColor="rgba(255, 255, 255, 0.5)"
          secureTextEntry={!isPasswordVisible}
          value={password}
          onChangeText={(text)=> setPassword(text)}
        />
        <TouchableOpacity style={styles.eyeIcon} onPress={togglePasswordVisibility}>
          <Icon name={isPasswordVisible ? "eye" : "eye-slash"} size={20} color="rgba(255, 255, 255, 0.5)" />
        </TouchableOpacity>
      </View>

      {/* login button */}

      <TouchableOpacity style={styles.buttonContainer} onPress={handleLogin}>
        <Text style={styles.buttonText}>Login</Text>
      </TouchableOpacity>

    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 20,
    backgroundColor: '#1A1B18',
  },
  imageContainer: {
    alignItems: 'center',
    marginBottom: 20,
  },
  image: {
    width: 800,
    height: 380,
    aspectRatio: 1,
    borderBottomLeftRadius: 50,
    borderBottomRightRadius: 50,
  },
  login: {
    textAlign: 'center',
    color: 'white',
    fontSize: 15,
    fontWeight: '600',
    marginBottom: 30,
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 49,
    borderColor: '#242422',
    marginBottom: 20,
  },
  inputIcon: {
    position: 'absolute',
    left: 20,
    zIndex: 1,
    backgroundColor: '#242422',
    paddingHorizontal: 10,
    borderRadius: 5,
  },
  input: {
    height: 49,
    borderColor: '#242422',
    paddingLeft: 60,
    backgroundColor: '#242422',
    color: '#FFFFFF',
    flex: 1,
    borderRadius: 20,
  },
  eyeIcon: {
    position: 'absolute',
    right: 20,
    zIndex: 1,
  },
  forgotPassword: {
    color: 'rgba(255, 255, 255, 0.5)',
    textAlign: 'right',
    marginBottom: 20,
    marginTop: -10,
  },
  buttonContainer: {
    backgroundColor: '#CBE877',
    paddingVertical: 13,
    borderRadius: 25,
    marginBottom: 30,
  },
  buttonText: {
    color: '#1A1B18',
    fontSize: 18,
    textAlign: 'center',
    fontWeight: '600',
  },
  signupContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: -10,
    marginBottom: 50,
  },
  signupPrompt: {
    color: '#818181',
  },
  highlight: {
    color: '#CBE877',
    fontWeight: 'bold',
  },
  errorMessage: {
    color: 'red',
    marginBottom: 10,
  },
});

export default LoginPage;