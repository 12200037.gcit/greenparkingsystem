import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Button, Alert, Modal } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { Ionicons } from '@expo/vector-icons';
import { BackHandler } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Toast from 'react-native-toast-message';
import { ip } from '../../utils/config';

const CurrentParkingDetailScreen = ({route}) => {
  const { marker, area, setTodo} = route.params;
  const navigation = useNavigation();
  const [modalVisible, setModalVisible] = useState(false);
  const [selectedReason, setSelectedReason] = useState('');
  
  const handleDoneLast = async (p_id) => {
    try {
      const token = await AsyncStorage.getItem('token');
      
      // Validate selectedReason
      if (!selectedReason) {
        throw new Error('Please select a reason for cancellation');
      }

      let requestBody = {
        selectedReason,
        a_id: marker.a_id,
        user_id: marker.user_id,
        user_name: marker.user_name
      };

      const response = await fetch(`http://${ip}:5000/collector/cancel/${p_id}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'token':token
        },
        body: JSON.stringify(requestBody), // Pass requestBody directly
      });
      
  
      if (!response.ok) {
        const errorData = await response.json();
        throw new Error(errorData); // Throw backend error message
      }

      console.log('Reason for cancellation:', selectedReason);
      setModalVisible(false);
      setTodo(true)

      // Handle successful registration
      navigation.navigate('HomeS');
      
      Toast.show({
        type: 'success',
        text1: 'Successfully cancelled',
        visibilityTime: 3000,
      });

    } catch (error) {
      Alert.alert('Cancel parking Error', error.message);
    }
  };

  //phone back button
  useEffect(() => {
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      () => {
        navigation.goBack();
        return true; // Prevent default behavior (exit the app)
      }
    );

    return () => backHandler.remove(); // Cleanup on unmount
  }, []);


  const handleBackPress = () => {
    navigation.goBack();
  };

  const handleCancelParking = () => {
    setModalVisible(true);
  };

   //time convertion
   const convertTo12HourTime = (timeString) => {
    if (!timeString) {
      return 'N/A'; // Or any default value to indicate that the time is not available
    }
  
    const [hours, minutes] = timeString.split(':').map(Number);
    const period = hours >= 12 ? 'PM' : 'AM';
    const displayHours = hours % 12 || 12;
    return `${displayHours}:${minutes < 10 ? '0' : ''}${minutes} ${period}`;
  };

  function calculateTimer(starttime) {
    // Split the starttime string into its components
    const [timePart, millisecondsPart] = starttime.split('.');
    const [hours, minutes, seconds] = timePart.split(':').map(Number);
    const milliseconds = millisecondsPart ? parseInt(millisecondsPart) : 0;

    // Create a new Date object with the given time components
    const startTimeDate = new Date();
    startTimeDate.setHours(hours, minutes, seconds, milliseconds);

    // Get the current time
    const currentTime = new Date();

    // Calculate the difference in milliseconds
    const difference = currentTime.getTime() - startTimeDate.getTime();

    // Calculate hours, minutes, and seconds from the difference
    const diffHours = Math.floor(difference / (1000 * 60 * 60));
    const diffMinutes = Math.floor((difference % (1000 * 60 * 60)) / (1000 * 60));
    const diffSeconds = Math.floor((difference % (1000 * 60)) / 1000);

    // Format the timer
    return `${String(diffHours).padStart(2, '0')}:${String(diffMinutes).padStart(2, '0')}:${String(diffSeconds).padStart(2, '0')}`;
}

  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={handleBackPress} style={styles.backButton}>
        <Ionicons name="arrow-back" size={24} color="white" />
        <Text style={styles.title}>Current Parking Details</Text>
      </TouchableOpacity>
      <View style={styles.detail}>
        <Text style={styles.t}>Contains the details about the car parking with following information </Text>
      </View>
      <View style={styles.a}>
        <Text style={styles.text}>Name:</Text>
        <Text style={styles.text1}>{marker.user_name}</Text>
      </View>
      <View style={styles.a}>
      <Text style={styles.text}>Area: </Text>
      <Text style={styles.text1}>{marker.area}</Text>
      </View>
      <View style={styles.a}>
        <Text style={styles.text}>Vehicle No: </Text>
        <Text style={styles.text1}>{marker.vehicleno}</Text>
      </View>
      <View style={styles.a}>
      <Text style={styles.text}>Start Time:</Text>
        <Text style={styles.text1}>{convertTo12HourTime(marker.starttime)}</Text>
      </View>
      <View style={styles.a}>
      <Text style={styles.text}>Total Time:</Text>
        <Text style={styles.text1}>45 mins(not yet)</Text>
      </View>
      <View style={styles.a}>
      <Text style={styles.text}>Total Amount:</Text>
        <Text style={styles.text1}>Nu.50/-(not yet)</Text>
      </View>

      <TouchableOpacity onPress={handleCancelParking} style={styles.cancelButton}>
        <Text style={styles.cancelButtonText}>Cancel Parking</Text>
      </TouchableOpacity>

      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(false);
        }} >
        <View style={styles.modalContainer}>
          <View style={styles.modalContent}>
            <Text style={styles.modalTitle}>Reason for cancelling</Text>
            
            <TouchableOpacity
              style={styles.radioButton}
              onPress={() => setSelectedReason('Run Away')} >
              <Ionicons
                name={selectedReason === 'Run Away' ? 'radio-button-on' : 'radio-button-off'}
                size={24}
                color="white"
              />
              <Text style={styles.radioButtonText}>Run Away</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.radioButton}
              onPress={() => setSelectedReason('Mistake Parking')}
            >
              <Ionicons
                name={selectedReason === 'Mistake Parking' ? 'radio-button-on' : 'radio-button-off'}
                size={24}
                color="white"
              />
              <Text style={styles.radioButtonText}>Mistake Parking</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.radioButton}
              onPress={() => setSelectedReason('Not Parked')}>
              <Ionicons
                name={selectedReason === 'Not Parked' ? 'radio-button-on' : 'radio-button-off'}
                size={24}
                color="white"/>
              <Text style={styles.radioButtonText}>Not Parked</Text>
            </TouchableOpacity>

              {/* for button */}
            <TouchableOpacity onPress={() => handleDoneLast(marker.p_id)} style={styles.cancelButton}>
              <Text style={styles.cancelButtonText}>Done</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>

    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1A1B18',
    paddingTop: 20,
    paddingLeft: 20,
    paddingRight: 20,
    color: 'white',
  },
  backButton: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    borderRadius: 5,
  },
  backButtonText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'white',
    marginLeft: 5,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'white',
    borderBlockColor: 'red',
    paddingHorizontal: 10,
  },
  a: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '100%',
    paddingVertical: 15,
    borderRadius: 10,
    backgroundColor: '#333',
    marginVertical: 5,
    marginBottom: 10,
    color: '#FFFFFF',
  },
  t: {
    fontSize: 15,
    color: 'grey',
    flexBasis: 'auto',
    justifyContent: 'center',
    paddingVertical: 10,
  },
  text: {
    color: 'white',
    flexDirection:'row',
    flex:1,
    marginLeft:50,
  },
  text1:{
    color: 'white',
    paddingRight:40,
    flex:1,
    textAlign:'left'
  },
  cancelButton: {
    backgroundColor: '#CBE877',
    borderRadius: 20,
    width: '100%',
    padding: 15,
    alignItems: 'center',
    marginVertical: 10,
  },
  cancelButtonText: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 16,
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modalContent: {
    backgroundColor: 'black',
    padding: 20,
    borderRadius: 10,
    width: '80%',
  },
  modalTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 10,
    color:'white'
  },
  radioButton: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 5,
    color:'rgba(255, 255, 255, 0.5)'
  },
  radioButtonText: {
    fontSize: 16,
    marginLeft: 10,
    color:'rgba(255, 255, 255, 0.5)'
  },
});

export default CurrentParkingDetailScreen;
