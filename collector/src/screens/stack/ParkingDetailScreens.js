import React, { useState } from 'react';
import { StyleSheet, Text, View, TouchableOpacity,Button, Alert, Modal } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { Ionicons } from '@expo/vector-icons';
import { ip } from '../../utils/config';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Toast from 'react-native-toast-message';
    
const ParkingDetailScreens = ({route}) => {
    const navigation = useNavigation();
    const [modalVisible, setModalVisible] = useState(false);
    const [selectedReason, setSelectedReason] = useState('scan');

    const { detail, setTodo } = route.params;
    // console.log(detail)

    const handleProceed = () => {
      setModalVisible(true);
    };
    const handleBackPress = () => {
      navigation.goBack();
    }; 

    //clear notification and history
    const handleDone = async (n_id) => {
      try {
        if (selectedReason === 'scan') {
          // Handle cancellation logic here
          setModalVisible(false);
          navigation.navigate('QRcode', { detail, setTodo });
        } else {
          const token = await AsyncStorage.getItem('token');

          let requestBody = {
            a_id: detail.a_id,
            area: detail.area,
            user_id: detail.user_id,
            user_name: detail.user_name,
            vehicleno: detail.vehicleno,
            starttime: detail.starttime,
            endtime: detail.endtime,
          };
    
          const response = await fetch(`http://${ip}:5000/collector/payment/${n_id}`, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              'token': token
            },
            body: JSON.stringify(requestBody),
          });
    
          if (!response.ok) {
            const errorData = await response.json();
            throw new Error(errorData); // Throw backend error message
          }
    
          setModalVisible(false);
          setTodo(true);
          navigation.navigate('Notification');
          Toast.show({
            type: 'success',
            text1: 'Payment Done by Cash',
            visibilityTime: 3000,
          });
        }
      } catch (error) {
        console.error('Error ', error);

        Alert.alert('Payment', error.message);
      }
    };
    

   //time convertion
   const convertTo12HourTime = (timeString) => {
    if (!timeString) {
      return 'N/A'; // Or any default value to indicate that the time is not available
    }
    const [hours, minutes] = timeString.split(':').map(Number);
    const period = hours >= 12 ? 'PM' : 'AM';
    const displayHours = hours % 12 || 12;
    return `${displayHours}:${minutes < 10 ? '0' : ''}${minutes} ${period}`;
  };

  function formatDuration(milliseconds) {
    const hours = Math.floor(milliseconds / 3600000);
    const minutes = Math.floor((milliseconds % 3600000) / 60000);
    return `${hours}h ${minutes}m`;
  }
  function formatDuration(durationString) {
    const [hoursStr, minutesStr, secondsStr] = durationString.split(':');
  
    const hours = parseInt(hoursStr, 10) * 3600000;
    const minutes = parseInt(minutesStr, 10) * 60000;
    const seconds = parseFloat(secondsStr) * 1000;
  
    const totalMilliseconds = hours + minutes + seconds;
  
    const formattedHours = Math.floor(totalMilliseconds / 3600000);
    const formattedMinutes = Math.floor((totalMilliseconds % 3600000) / 60000);
  
    if (formattedHours === 0) {
      return `${formattedMinutes}m`;
    } else {
      return `${formattedHours}h ${formattedMinutes}m`;
    }
  }
  return (
    <View style={styles.container}>
    <TouchableOpacity onPress={handleBackPress} style={styles.backButton}>
      <Ionicons name="arrow-back" size={24} color="white" />
      <Text style={styles.title}>Parking Details</Text>
    </TouchableOpacity>
    <View style={styles.detail}>
      <Text style={styles.t}>Contains the details about the car parking with following information </Text>
    </View>

    {/* Notification Details */}
    <View style={styles.a}>
      <Text style={styles.text}>Name:</Text>
      <Text style={styles.text1}>{detail.user_name}</Text>
    </View>
    <View style={styles.a}>
    <Text style={styles.text}>Vehicle:</Text>
      <Text style={styles.text1}>{detail.vehicleno}</Text>
    </View>
    <View style={styles.a}>
      <Text style={styles.text}>Start Time:</Text>
      <Text style={styles.text1}>{convertTo12HourTime(detail.starttime)}</Text>
    </View>
    <View style={styles.a}>
      <Text style={styles.text}>End Time:</Text>
      <Text style={styles.text1}>{convertTo12HourTime(detail.endtime)}</Text>
    </View>
    <View style={styles.a}>
      <Text style={styles.text}>Total hours:</Text>
      <Text style={styles.text1}>{formatDuration(detail.duration)}</Text>
    </View>
    <View style={styles.a}>
      <Text style={styles.text}>Total Amount:</Text>
      <Text style={styles.text1}>Nu.100/-</Text>
    </View>
    <TouchableOpacity onPress={handleProceed} style={styles.cancelButton}>
        <Text style={styles.cancelButtonText}>Proceed to payment</Text>
      </TouchableOpacity>

    <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(false);
        }}
      >
        <View style={styles.modalContainer}>
          <View style={styles.modalContent}>
            <Text style={styles.modalTitle}>Choose Payment Method</Text>

            <TouchableOpacity
              style={styles.radioButton}
              onPress={() => setSelectedReason('scan')} >
              <Ionicons
                name={selectedReason === 'scan' ? 'radio-button-on' : 'radio-button-off'}
                size={24}
                color="white" />
              <Text style={styles.radioButtonText}>Scan QR Code</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.radioButton}
              onPress={() => setSelectedReason('cash')}    >
              <Ionicons
                name={selectedReason === 'cash' ? 'radio-button-on' : 'radio-button-off'}
                size={24}
                color="white"    />
              <Text style={styles.radioButtonText}>Pay with Cash</Text>
            </TouchableOpacity>
            
              {/* for button */}
            <TouchableOpacity onPress={()=>handleDone(detail.n_id)} style={styles.cancelButton}>
              <Text style={styles.cancelButtonText}>Done</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
</View>
  )
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'black',
      paddingTop: 20,
      paddingLeft: 20,
      paddingRight: 20,
      color: 'white',
    },
    backButton: {
      flexDirection: 'row',
      alignItems: 'center',
      padding: 10,
      borderRadius: 5,
    },
    backButtonText: {
      fontSize: 16,
      fontWeight: 'bold',
      color: 'white',
      marginLeft: 5,
    },
    title: {
      fontSize: 20,
      fontWeight: 'bold',
      color: 'white',
      paddingHorizontal: 10,
    },
    a: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      width: '100%',
       paddingVertical: 13,
      borderRadius: 10,
      backgroundColor: '#333',
      marginVertical: 5,
      marginBottom: 5,
      color: '#FFFFFF',
    },
    t: {
      fontSize: 15,
      color: 'grey',
      flexBasis: 'auto',
      justifyContent: 'center',
      paddingVertical: 10,
    },
    text: {
      color: 'white',
      flexDirection:'row',
      flex:1,
      marginLeft:40,
    },
    text1:{
      color: 'white',
      paddingRight:40,
      flex:1,
      textAlign:'left'
    },
    cancelButton: {
      backgroundColor: '#CBE877',
      borderRadius: 20,
      width: '100%',
      padding: 15,
      alignItems: 'center',
      marginVertical: 20,
    },
    cancelButtonText: {
      color: 'black',
      fontWeight: 'bold',
      fontSize: 16,
      fontWeight:'bold'
    },
    modalContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
    modalContent: {
      backgroundColor: 'black',
      padding: 20,
      borderRadius: 10,
      width: '80%',
    },
    modalTitle: {
      fontSize: 18,
      fontWeight: 'bold',
      marginBottom: 10,
      color:'white'
    },
    radioButton: {
      flexDirection: 'row',
      alignItems: 'center',
      marginVertical: 5,
      color:'white'
    },
    radioButtonText: {
      fontSize: 16,
      marginLeft: 10,
      color:'rgba(255, 255, 255, 0.5)'
    },
  });
  
export default ParkingDetailScreens;