import React, { useState, useEffect } from 'react';
import { StyleSheet, Image, Text, View, TouchableOpacity, Button, Alert, Modal, TextInput } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { Ionicons } from '@expo/vector-icons';
import { BackHandler } from 'react-native';

const HistoryDetailScreen = ({route}) => {

    const { marker } = route.params;
    const navigation = useNavigation();
    
    const handleBackPress = () => {
        navigation.goBack();
      };

      useEffect(() => {
        const backHandler = BackHandler.addEventListener(
          'hardwareBackPress',
          () => {
            navigation.goBack();
            return true; // Prevent default behavior (exit the app)
          }
        );
    
        return () => backHandler.remove(); // Cleanup on unmount
      }, []);

  
     //time convertion
  const convertTo12HourTime = (timeString) => {
    if (!timeString) {
      return 'N/A'; // Or any default value to indicate that the time is not available
    }
  
    const [hours, minutes] = timeString.split(':').map(Number);
    const period = hours >= 12 ? 'P.M' : 'A.M';
    const displayHours = hours % 12 || 12;
    return `${displayHours}:${minutes < 10 ? '0' : ''}${minutes} ${period}`;
  };

  //date convertion
  const formatDate = (dateString) => {
    const date = new Date(dateString);
    const options = { day: 'numeric', month: 'long', year: 'numeric' };
    return date.toLocaleDateString('en-GB', options);
  };

  //duration convertion
  const formatDuration = (timeString) => {
    if (!timeString) {
      return 'N/A'; // Or any default value to indicate that the time is not available
    }
    // Split the time string into hours, minutes, and seconds
    const [hours, minutes] = timeString.split(':').map(Number);
  
    // Construct the formatted string
    let formattedString = '';
    if (hours > 0) {
      formattedString += `${hours}h `;
    }
    if (minutes > 0 || !formattedString) {
      formattedString += `${minutes}m`;
    }
    return formattedString.trim();
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={handleBackPress} style={styles.backButton}>
        <Ionicons name="arrow-back" size={24} color="white" />
        <Text style={styles.title}>History Details</Text>
      </TouchableOpacity>

        <View style={styles.detail}>
            <Text style={styles.t}>Contains the details
             about the car parking with following information </Text>
        </View>
        
        <View style={styles.a}>
            <View style = {styles.r}>
              <Text style={styles.text}>Name</Text>
              <Text style={styles.text1}>: {marker.user_name}</Text>
            </View>
            <View style = {styles.r}>
              <Text style={styles.text}>Vehicle No.</Text>
              <Text style={styles.text1}>: {marker.vehicleno}</Text>
            </View>
            <View style = {styles.r}>
              <Text style={styles.text}>Date</Text>
              <Text style={styles.text1}>: {formatDate(marker.date)}</Text>
            </View>
            <View style = {styles.r}>
              <Text style={styles.text}>Start Time</Text>
              <Text style={styles.text1}>: {convertTo12HourTime(marker.starttime)}</Text>
            </View>
            <View style = {styles.r}>
              <Text style={styles.text}>End Time</Text>
              <Text style={styles.text1}>: {convertTo12HourTime(marker.endtime)}</Text>
            </View>
            <View style = {styles.r}>
              <Text style={styles.text}>Total Time</Text>
              <Text style={styles.text1}>: {formatDuration(marker.duration)}</Text>
            </View>
            <View style = {styles.r}>
              <Text style={styles.text}>Total Amount</Text>
              <Text style={styles.text1}>: Nu.{marker.totalamount}/-</Text>
            </View>
          </View>
        </View>
  );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#0D0907',
        paddingTop: 20,
        paddingLeft: 20,
        paddingRight: 20,
        color: 'white',
      },
      backButton: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 10,
        borderRadius: 5,
      },
      title: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'white',
        paddingHorizontal: 10,
      },
      t: {
        fontSize: 15,
        color: 'grey',
        flexBasis: 'auto',
        justifyContent: 'center',
        paddingVertical: 10,
      },
      a:{
        flexDirection:'column',
        width:'100%',
        height:'60%',
        paddingVertical:19,
        borderRadius:10,
        backgroundColor:'#333',
        marginTop:15,
        paddingHorizontal:25
      },
      r:{
        flexDirection:'row',
        paddingVertical:10,
        
      },
      text:{
        color:'white',
        flexDirection:'row',
        flex:1,
      },
      text1:{
        color:'white',
        flex:1,
        textAlign:'left'
      },
});

export default HistoryDetailScreen;