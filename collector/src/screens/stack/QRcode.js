
import React, { useState } from 'react';
import { StyleSheet, Image, Text, View, TouchableOpacity, Button, Alert, Modal, TextInput } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { Ionicons } from '@expo/vector-icons';
import { ip } from '../../utils/config';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Toast from 'react-native-toast-message';

const QRcodeScreen = ({route}) => {
  const { detail, setTodo } = route.params;
  // console.log(setTodo)
  const navigation = useNavigation();
  const [modalVisible, setModalVisible] = useState(false);
  const [selectedReason, setSelectedReason] = useState('');
  const [feedbackText, setFeedbackText] = useState('');

  const handleBackPress = () => {
    navigation.goBack();
  };

  // finish payment
  const handleFinish = async (n_id) => {
    try {
      const token = await AsyncStorage.getItem('token');

      let requestBody = {
        a_id: detail.a_id,
        area: detail.area,
        user_id: detail.user_id,
        user_name: detail.user_name,
        vehicleno: detail.vehicleno,
        starttime: detail.starttime,
        endtime: detail.endtime,
        // paymenttime : 'scan',
      };

      const response = await fetch(`http://${ip}:5000/collector/payment/${n_id}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'token': token
        },
        body: JSON.stringify(requestBody),
      });

      if (!response.ok) {
        const errorData = await response.json();
        throw new Error(errorData); // Throw backend error message
      }

      // setModalVisible(true); //feedback
      navigation.navigate('Home');
      setTodo(true);
      Toast.show({
        type: 'success',
        text1: 'Payment Done by Scan',
        visibilityTime: 3000,
      });
    
    } catch (error) {
      Alert.alert('Payment', error.message);
    }
  };

  const handleOptionSelect = (reason) => {
    setSelectedReason(reason);
  };

  //feedback
  const handleDone = () => {
    if (selectedReason) {
      // Handle logic here
      // console.log('Selected Reason:', selectedReason);
      // console.log('Feedback Text:', feedbackText);

      setModalVisible(false);
      // setTodo(true)
      navigation.navigate('Home');
    } else {
      Alert.alert('Please select a reason for feedback');
    }
  };

  const handleSkip = () => {
    setModalVisible(false);
    navigation.navigate('Notification');
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={handleBackPress} style={styles.backButton}>
        <Ionicons name="arrow-back" size={24} color="white" />
        <Text style={styles.title}>Company QR Code</Text>
      </TouchableOpacity>
      <View style={styles.detail}>
        <Text style={styles.t}>Scan the QR code</Text>
      </View>
      <View style={styles.img}>
        <Image
          source={require('../../../assets/qr.png')}
          style={styles.image}
          resizeMode="contain"
        />
      </View>

      <TouchableOpacity onPress={()=> handleFinish(detail.n_id)} style={styles.finishButton}>
        <Text style={styles.finishButtonText}>Finish</Text>
      </TouchableOpacity>

      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => setModalVisible(false)}
      >
        <View style={styles.modalContainer}>
          <View style={styles.modalContent}>
            <Text style={styles.modalTitle}>Give feedback to the User</Text>
            <TouchableOpacity style={styles.optionButton} onPress={() => handleOptionSelect('Polite')}>
              <Ionicons
                name={selectedReason === 'Polite' ? 'radio-button-on' : 'radio-button-off'}
                size={12}
                color="white"
              />
              <Text style={styles.optionButtonText}>Polite</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.optionButton} onPress={() => handleOptionSelect('Verbal Harassment')}>
              <Ionicons
                name={selectedReason === 'Verbal Harassment' ? 'radio-button-on' : 'radio-button-off'}
                size={12}
                color="white"
              />
              <Text style={styles.optionButtonText}>Verbal Harassment</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.optionButton} onPress={() => handleOptionSelect('Vulgar Language')}>
              <Ionicons
                name={selectedReason === 'Vulgar Language' ? 'radio-button-on' : 'radio-button-off'}
                size={12}
                color="white"
              />
              <Text style={styles.optionButtonText}>Vulgar Language</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.optionButton} onPress={() => handleOptionSelect('Punctual')}>
              <Ionicons
                name={selectedReason === 'Punctual' ? 'radio-button-on' : 'radio-button-off'}
                size={12}
                color="white"
              />
              <Text style={styles.optionButtonText}>Punctual</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.optionButton} onPress={() => handleOptionSelect('Rude Behavior')}>
              <Ionicons
                name={selectedReason === 'Rude Behavior' ? 'radio-button-on' : 'radio-button-off'}
                size={15}
                color="white"
              />
              <Text style={styles.optionButtonText}>Rude Behavior</Text>
            </TouchableOpacity>

            <TextInput
              style={styles.feedbackInput}
              placeholder="Enter feedback..."
              onChangeText={setFeedbackText}
              value={feedbackText}
              multiline={true}
              numberOfLines={4}
              placeholderTextColor="white"/>

            <TouchableOpacity onPress={handleDone} style={styles.doneButton}>
              <Text style={styles.doneButtonText}>Done</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={handleSkip} style={styles.skipButton}>
              <Text style={styles.skipButtonText}>Skip</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#0D0907',
    paddingTop: 20,
    paddingLeft: 20,
    paddingRight: 20,
    color: 'white',
  },
  backButton: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    borderRadius: 5,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'white',
    paddingHorizontal: 10,
  },
  detail: {
    marginBottom: 20,
  },
  t: {
    fontSize: 15,
    color: 'grey',
    justifyContent: 'center',
  },
  img: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    height: 300,
    width: 300,
    marginBottom: 10,
  },
  finishButton: {
    backgroundColor: '#CBE877',
    borderRadius: 20,
    width: '100%',
    padding: 15,
    alignItems: 'center',
    marginVertical: 20,
  },
  finishButtonText: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 16,
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modalContent: {
    backgroundColor: 'black',
    padding: 20,
    borderRadius: 10,
    width: '80%',
  },
  modalTitle: {
    fontSize: 17,
    fontWeight: 'bold',
    marginBottom: 10,
    color: 'white',
  },
  optionButton: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 5,
    color: 'white',
  },
  optionButtonText: {
    fontSize: 16,
    marginLeft: 10,
    color: 'white',
  },
  doneButton: {
    backgroundColor: '#CBE877',
    borderRadius: 20,
    width: '100%',
    padding: 15,
    alignItems: 'center',
    marginTop: 10,
  },
  doneButtonText: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 16,
  },
  skipButton: {
    padding: 15,
    alignItems: 'center',
    marginTop: 10,
    marginVertical:'20'
  },
  skipButtonText: {
    fontWeight: 'bold',
    fontSize: 12,
    color:'white',
  },
  feedbackInput: {
    backgroundColor: 'grey',
    borderRadius: 10,
    padding: 10,
    color:'black'
  },
});

export default QRcodeScreen;
