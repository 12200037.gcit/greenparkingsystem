import React, { useState, useEffect } from 'react';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import CollectorTab from '../navigation/CollectorTab';
import LoginPage from '../screens/auth/Login';
import NotificationScreen from '../screens/tab/Notification';
import CurrentParkingDetailScreen from '../screens/stack/CurrentParkingDetailScreen';
import QRcodeScreen from '../screens/stack/QRcode';
import ParkingDetailScreens from '../screens/stack/ParkingDetailScreens'
import HistoryScreen from '../screens/tab/HistoryScreen';
import HistoryDetailScreen from '../screens/stack/HistoryDetailScreen';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { LogBox } from 'react-native';
import { ip } from '../utils/config';

LogBox.ignoreLogs([
  'Non-serializable values were found in the navigation state'
]);

const Stack = createStackNavigator();

const CollectorStack = () => {

const [isAuthenticated, setIsAuthenticated] = useState(false);

const checkAuthenticated = async () => {
  try {
    const token = await AsyncStorage.getItem('token');
    const response = await fetch(`http://${ip}:5000/auth/verify`, {
      method: "GET",
      headers: { 
        'Content-Type': 'application/json',
        'token': token 
      }
    });

    const parseRes = await response.json();

    parseRes === true ? setIsAuthenticated(true) : setIsAuthenticated(false);
  } catch (error) {
    console.error(error.message);
  }
};

useEffect(() => {
  checkAuthenticated();
}, []);

  return (
    <Stack.Navigator    
      screenOptions={{
          headerShown: false,
          gestureEnabled: false,
          ...TransitionPresets.SlideFromRightIOS, // Use SlideFromRightIOS transition
        }}>
       {!isAuthenticated ? (
                    <>
                      <Stack.Screen
                        name="Login"
                        component={LoginPage}
                        options={{ headerShown: false}}
                        initialParams={{ setIsAuthenticated }}
                      />
                    </>
                  ) : (
                    <>
                  
                      <Stack.Screen 
                        name="HomeS" 
                        component={CollectorTab} 
                        initialParams={{ setIsAuthenticated }}
                      />

                      <Stack.Screen 
                        name="CurrentParkingDetails" 
                        component={CurrentParkingDetailScreen}  
                        options={{ headerShown: false }} 
                        initialParams={{ setIsAuthenticated }}
                      />
                      <Stack.Screen 
                        name="ParkingDetails" 
                        component={ParkingDetailScreens}  
                        options={{ headerShown: false }} 
                        initialParams={{ setIsAuthenticated }}
                      />
                      <Stack.Screen 
                        name="QRcode" 
                        component={QRcodeScreen}  
                        options={{ headerShown: false }}
                        initialParams={{ setIsAuthenticated }}
                      />
                      <Stack.Screen 
                        name="HistoryDetail" 
                        component={HistoryDetailScreen} 
                        initialParams={{ setIsAuthenticated }}
                      />
                    </>
                  )}

      </Stack.Navigator> 
  )
}

export default CollectorStack