import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { useNavigation } from '@react-navigation/native'; 
import HomeScreen from '../screens/tab/Home';
import NotificationScreen from '../screens/tab/Notification';
import { Ionicons } from '@expo/vector-icons';
import HistoryScreen from '../screens/tab/HistoryScreen';
import CurrentParkingDetailScreen from '../screens/stack/CurrentParkingDetailScreen';

const Tab = createBottomTabNavigator();

const CollectorTab = ({route}) => {
  
const {setIsAuthenticated} = route.params;
const navigation = useNavigation();

return (
    <Tab.Navigator 
      screenOptions={{
        headerShown: false,
        tabBarActiveTintColor: '#CBE877', 
        tabBarStyle: {
          backgroundColor: '#242422',
          borderColor:'#242422',
        },
      }}
    >
      <Tab.Screen
        name="Home"
        component={HomeScreen}
        initialParams={{ setIsAuthenticated }}
        options={{
          tabBarIcon: ({ color, size }) => (
            <Ionicons name="home-outline" size={size} color={color} />
          ),
        }}
      />
        <Tab.Screen
        name="Notification"
        initialParams={{ setIsAuthenticated }}
        component={NotificationScreen}
        options={{
          tabBarIcon: ({ color, size }) => (
            <Ionicons name="notifications" size={size} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name="History"
        initialParams={{ setIsAuthenticated }}
        component={HistoryScreen}
        options={{
          tabBarIcon: ({ color, size }) => (
            <Ionicons name="time-outline" size={size} color={color} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default CollectorTab;
